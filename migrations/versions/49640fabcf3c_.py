"""empty message

Revision ID: 49640fabcf3c
Revises: 5ed1e3e95bc
Create Date: 2018-10-22 16:25:58.785742

"""

# revision identifiers, used by Alembic.
revision = '49640fabcf3c'
down_revision = '5ed1e3e95bc'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():

    op.create_table('ceilings_version',
    sa.Column('id', sa.Integer(), autoincrement=False, nullable=False),
    sa.Column('code', sa.String(length=80), autoincrement=False, nullable=True),
    sa.Column('year', sa.Integer(), autoincrement=False, nullable=True),
    sa.Column('ceiling', sa.Float(), autoincrement=False, nullable=True),
    sa.Column('timestamp', sa.DateTime(), autoincrement=False, nullable=True),
    sa.Column('transaction_id', sa.BigInteger(), autoincrement=False, nullable=False),
    sa.Column('end_transaction_id', sa.BigInteger(), nullable=True),
    sa.Column('operation_type', sa.SmallInteger(), nullable=False),
    sa.Column('code_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('year_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('ceiling_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.Column('timestamp_mod', sa.Boolean(), server_default=sa.text(u'false'), nullable=False),
    sa.PrimaryKeyConstraint('id', 'transaction_id', name=op.f('pk_ceilings_version'))
    )
    op.create_index(op.f('ix_ceilings_version_end_transaction_id'), 'ceilings_version', ['end_transaction_id'], unique=False)
    op.create_index(op.f('ix_ceilings_version_operation_type'), 'ceilings_version', ['operation_type'], unique=False)
    op.create_index(op.f('ix_ceilings_version_transaction_id'), 'ceilings_version', ['transaction_id'], unique=False)
    op.create_table('ceilings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('code', sa.String(length=80), nullable=True),
    sa.Column('year', sa.Integer(), nullable=True),
    sa.Column('ceiling', sa.Float(), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_ceilings')),
    sa.UniqueConstraint('code', name=op.f('uq_ceilings_code')),
    schema='epr'
    )


def downgrade():

    op.drop_table('ceilings', schema='epr')
    op.drop_index(op.f('ix_ceilings_version_transaction_id'), table_name='ceilings_version')
    op.drop_index(op.f('ix_ceilings_version_operation_type'), table_name='ceilings_version')
    op.drop_index(op.f('ix_ceilings_version_end_transaction_id'), table_name='ceilings_version')
    op.drop_table('ceilings_version')
