
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

try:
    from .BaseTestClass import *
except ImportError:
    from BaseTestClass import *

log = logging.getLogger( "Testing.test_shiftTaskMap" )
log.setLevel(logging.DEBUG)

class ShiftTaskMapTestCase(BaseTestCase):

    def createEntry( self ) :
        data = { 'subSystem'   : 'pr2',
                 'shiftType'   : 'sType3',
                 'flavourName' : 'main',
                 'taskCode'    : 'ts1',
                 'year'        : self.selYear,
                 }
        response = self.client.post( url_for( 'api.newShiftTaskMap' ),
                                     data=data,
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        json_response = json.loads( response.data.decode( 'utf-8' ) )
        # log.info( '++> response.data: %s' % response.data )
        self.assertIn( 'OK', json_response[ 'result' ] )

        id = json_response[ 'id' ]

        return response, id

    def delEntry( self, id ) :

        response = self.client.delete( url_for( 'api.delShiftTaskMap' ),
                                       data = { 'id' : id },
                                     follow_redirects=True )
        self.assertEqual( response.status_code, 200 )

        json_response = json.loads( response.data.decode( 'utf-8' ) )
        # log.info( '--> response.data: %s' % response.data )

        self.assertIn( 'OK', json_response[ 'result' ] )
        self.assertIn( 'deleted', json_response[ 'msg' ] )

        return response, json_response

    def checkFail( self, data ) :
        self.login( )
        response = self.client.post( url_for( 'api.newShiftTaskMap' ),
                                     data=data,
                                     follow_redirects=True )

        self.assertEqual( response.status_code, 200 )

        log.info( '==> response.data: %s' % response.data )
        json_response = json.loads( response.data.decode( 'utf-8' ) )

        self.assertIn( 'ERROR', json_response[ 'result' ] )
        log.info( 'Correctly found error with msg: %s' % json_response[ 'msg' ] )

# =====================================================================

    def test_checkShiftsInDB(self):

        selYear = getPledgeYear()
        shiftsselYear = db.session.query(Shift).filter(Shift.year == selYear).all()
        self.assertEqual( len(shiftsselYear ), 34 )

        mapselYear = db.session.query(ShiftTaskMap).filter(ShiftTaskMap.year == selYear).all()
        self.assertEqual( len( mapselYear ), 2 )


    def test_shiftTaskMap(self):
        self.login()

        response, id = self.createEntry()

        response = self.client.get( url_for( 'api.shiftTaskMap', id=1 ),
                                    follow_redirects=True )

        self.assertEqual( response.status_code, 200 )

        # log.info( 'response.data: %s' % response.data )
        json_response = json.loads( response.data.decode( 'utf-8' ) )

        self.assertIn( 'OK', json_response[ 'result' ] )
        self.assertIn( 'pr2', json_response[ 'data' ]['subSystem'] )

        self.delEntry( id )

    def test_shiftTaskMapFail(self):
        self.login()

        # this ID should not exist:
        response = self.client.get( url_for( 'api.shiftTaskMap', id=42 ),
                                    follow_redirects=True )

        self.assertEqual( response.status_code, 200 )

        # log.info( 'response.data: %s' % response.data )
        json_response = json.loads( response.data.decode( 'utf-8' ) )

        self.assertIn( 'ERROR', json_response[ 'result' ] )
        self.assertIn( 'Exception when retrieving', json_response[ 'msg' ] )

    def test_newShiftTaskMap(self):
        self.login()

        response, id = self.createEntry()

        # log.info( '++> response.data: %s' % response.data )
        json_response = json.loads( response.data.decode( 'utf-8' ) )
        self.assertIn( 'OK', json_response['result'] )

        # now clean up again:
        self.delEntry(id)

    def test_mapShiftToTaskFail(self):

        # make sure we have one entry in the system:
        response, id = self.createEntry()

        # no task code
        self.checkFail( { 'subSystem' : '', 'shiftType' : '', 'flavourName' : '', 'taskCode' : '', 'year' : self.selYear, } )
        # wrong task code - not found
        self.checkFail( { 'subSystem' : '', 'shiftType' : '', 'flavourName' : '', 'taskCode' : 'ts2', 'year' : self.selYear, } )
        # wrong project - ts1 is a3 - p2
        self.checkFail( { 'subSystem' : 'p1', 'shiftType' : '', 'flavourName' : '', 'taskCode' : 'ts1', 'year' : self.selYear, } )
        # t1 is not a shift task
        self.checkFail( { 'subSystem' : '', 'shiftType' : '', 'flavourName' : '', 'taskCode' : 't1', 'year' : self.selYear, } )
        # no shift with this flavourname
        self.checkFail( { 'subSystem' : '', 'shiftType' : '', 'flavourName' : 'mainToo', 'taskCode' : 'ts1', 'year' : self.selYear, } )
        # duplicate:
        self.checkFail( { 'subSystem' : 'pr2', 'shiftType' : 'sType3', 'flavourName' : 'main', 'taskCode'  : 'ts1', 'year' : self.selYear, } )

        # now clean up again:
        self.delEntry(id)
