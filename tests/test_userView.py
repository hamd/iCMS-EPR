#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import re

try:
    from .BaseTestClass import *
except ImportError:
    from BaseTestClass import *

log = logging.getLogger( "Testing.test_userView" )
logging.basicConfig( stream=sys.stderr )
logging.getLogger( "Testing.test_userView" ).setLevel( logging.DEBUG )

class UserModelTestCase( BaseTestCase ):

    def doPage(self, endPoint, userName, data=None, isIn=[], isNotIn=[], isInRe=[], showPage=False, adminOK=False, isInAlert=None, headers=None, **kwargs):

        user = db.session.query(EprUser).filter_by( username=userName ).one( )
        if not adminOK: self.assertFalse( user.is_wizard() )
        self.login( user.username )

        if kwargs:
            response = self.client.get( url_for( endpoint=endPoint, **kwargs),
                                        data=data,
                                        headers = headers,
                                        follow_redirects=True )
        else:
            response = self.client.get( url_for( endpoint=endPoint ),
                                        data=data,
                                        headers = headers,
                                        follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        if showPage:
            log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, 200 )
        if not isInAlert:
            self.assertNotIn( 'class="alert alert-"', response.data.decode('utf-8'))
        else:
            for k in isInAlert:
                self.assertIn( k, response.data.decode('utf-8') )

        for k in isIn:
            self.assertIn( k, response.data.decode('utf-8') )
        for reK in isInRe:
            self.assertRegexpMatches( response.data.decode('utf-8'), reK )
        for k in isNotIn :
            self.assertNotIn( k, response.data.decode('utf-8') )

        self.logout()

    def doPagePost(self, endPoint, userName, data=None, isIn=[], isNotIn=[], showPage=False, adminOK=False, expectedRetCode=200, **kwargs):

        user = db.session.query(EprUser).filter_by( username=userName ).one( )
        if not adminOK: self.assertFalse( user.is_wizard() )
        self.login( user.username )

        if kwargs:
            response = self.client.post( url_for( endpoint=endPoint, **kwargs),
                                        data=data,
                                        follow_redirects=True )
        else:
            response = self.client.post( url_for( endpoint=endPoint ),
                                        data=data,
                                        follow_redirects=True )

        # log.info("got response status %s " % str(response.status) )
        if showPage:
            log.info( "got response data %s " % str( response.data.decode('utf-8') ) )

        self.assertEqual( response.status_code, expectedRetCode )

        for k in isIn:
            self.assertIn( k, response.data.decode('utf-8') )
        for k in isNotIn :
            self.assertNotIn( k, response.data.decode('utf-8') )

        self.logout()

        return response.data

    def test_showProjectInstAuthors( self ):
        self.doPage( endPoint='main.showProjectInstAuthors', userName='user1', projId=1,
                     data = None,
                     # showPage=True,
                     isIn=['iCMS - EPR Authors by main project for institutes in %s' % testYear, 'COMP', 'ETHZ', '5.00', '4.00'])

    def test_manageInstEprUsers(self):
        self.doPage( endPoint='manage.manageInstUsers', userName='user1', code='DESY',
                     data = None,
                     # showPage=True,
                     isIn=['Sorry, this action is not yet available'] )

    def test_changeEprUserInst(self):
        self.doPage( endPoint='manage.changeUserInst', userName='user1', cmsId=9900,
                     data = None,
                     # showPage=True,
                     isIn=['Sorry, you are not authorised for this action'] )

    def test_manageProjTasks( self ) :
        self.doPage( endPoint='manage.manageProjTasks', userName='user1', projName='pr1',
                        data=None,
                        # showPage=True,
                        isIn=[ 'Sorry you are not allowed to manage project' ] )

    def test_shiftWeightScales( self ) :
        self.doPage( endPoint='manage.shiftWeightScales', userName='user1',
                        data=None,
                        # showPage=True,
                        isIn=[ 'iCMS - EPR Set Weight-scales for Shifts' ] )

    def test_removeSTM( self ) :
        self.doPage( endPoint='manage.removeSTM', userName='user1', idReq=1,
                        data=None,
                        # showPage=True,
                        isInAlert=[ 'Sorry you are not allowed to manage shifts' ] )

    def test_roles_and_permissions(self):

        cern = db.session.query(EprInstitute).filter_by(code='CERN').one()

        admRole = db.session.query(Role).filter_by(permissions=Permission.ADMINISTER).first()
        ap = EprUser( username='andreasp', name='Pfeiffer, Andreas', hrId=4224051, cmsId=9991,
                      instId=cern.cernId, authorReq=True, status='CMS', isSusp=False, categoryName='Physicist',
                      role=admRole, year=self.selYear, mainInst=cern.id )
        self.assertTrue(ap.is_wizard())

        u = db.session.query(EprUser).filter_by( username='user1').one()
        self.assertFalse(u.is_administrator())

        adm = db.session.query(EprUser).filter_by( username='pfeiffer' ).one( )
        self.assertTrue( adm.is_wizard( ) )

        engOff = db.session.query( EprUser ).filter_by( username='engoffice' ).one()
        self.assertTrue( engOff.is_administrator() )
        self.assertFalse( engOff.is_wizard() )

    def test_anonymous_user(self):
        u = AnonymousUser()
        self.assertFalse(u.is_administrator())
        self.assertFalse(u.is_wizard())

    def test_setYear( self ) :
        self.doPage( endPoint='main.setYear', userName='user1', selYear=self.selYear,
                     isIn=[ 'iCMS - EPR Pledge Overview\n for user, one' ] )

    def test_check(self):
        res = self.doPage( endPoint='main.check', userName='user1',
                           isIn=[ 'iCMS - Check' ] )

    def test_faq(self):
        self.doPage( endPoint='main.faq', userName='user1',
                     isIn=[ 'Frequently Asked Questions', 'task XYZ' ] )

    def test_showMine(self):
        self.doPage(endPoint='main.showMine', userName='user1',
                    isIn=['iCMS - EPR Pledge Overview\n for user, one'] )

    def test_showMyCountry( self ) :
        self.doPage( endPoint='main.showMyCountry', userName='user1',
                    isIn=['iCMS - EPR Country Information for SWITZERLAND'] )

    def test_showMyInstPledges( self ) :
        self.doPage( endPoint='main.showMyInstPledges', userName='user1',
                     isIn=['iCMS - EPR Pledge Overview\n\n for CERN'])

    def test_showMyInst( self ) :
        self.doPage( endPoint='main.showMyInst', userName='user1' )

    def test_showall( self ) :
        self.doPage( endPoint='main.show_all', userName='user1' )

    def test_list( self ) :
        self.doPage( endPoint='main.listAll', userName='user1' )


    def test_showEprUser( self ) :
        self.doPage( endPoint='main.showUser', userName='user1',
                     isIn=[ 'iCMS - EPR Pledge Overview\n for user, two' ],
                     username='user2' )

    def test_findEprUser( self ) :
        self.doPage( endPoint='main.findUser', userName='user1',
                     isIn=[ 'iCMS - EPR', 'Select User:' ],
                     username='user2' )

    def test_showEprUserFail( self ) :
        self.doPage( endPoint='main.showUser', userName='user1',
                     isIn=[ 'Sorry, user with username notExisting not found in DB' ],
                     username='notExisting' )

    def test_show(self):
        self.doPage( endPoint='main.show', userName='user1',
                     isIn=['iCMS - EPR Pledge Overview'] )

    def test_showProject(self):
        self.doPage( endPoint='main.showProject', userName='user1', id=1,
                     isIn=['iCMS - EPR Pledge Overview \nfor pr1 '] )

    def test_showProjectTasks(self):
        self.doPage( endPoint='main.showProjectTasks', userName='user1', id=2,
                     isIn=['iCMS - EPR Tasks/Activities for  project pr2'] )

    def test_showShifts(self):
        self.doPage( endPoint='main.showShifts', userName='user1',
                     isIn=[ 'iCMS - EPR Shifts in %s' % self.selYear ] )

    def test_showInstitutes( self ) :
        self.doPage( endPoint='main.showInstitutes', userName='user1',
                     isIn=['iCMS - EPR Institute Overview in'] )

    def test_showInstitute( self ) :
        self.doPage( endPoint='main.showInstitute', userName='user1', code='FNAL',
                     isIn=['iCMS - EPR Institute Information for FNAL in'] )

    def test_showInstituteWGuestPl( self ) :
        self.doPage( endPoint='main.showInstitute', userName='user1', code='ETHZ',
                     isIn=['iCMS - EPR Institute Information for ETHZ in'] )

    def test_showInstituteLower( self ) :
        self.doPage( endPoint='main.showInstitute', userName='user1', code='Fnal',
                     isIn=['iCMS - EPR Institute Information for FNAL in'] )

    def test_showInstituteNonExist( self ) :
        self.doPage( endPoint='main.showInstitute', userName='user1', code='nonExistingInst',
                     isIn=['iCMS - EPR Institute Information for  in',
                           'alert alert-danger',
                           'No institute found for code'] )

    def test_showInstitutePost( self ) :
        self.doPagePost( endPoint='main.showInstitute', userName='user1',
                         data= { 'code' : 'FNAL' },
                         isIn=['iCMS - EPR Institute Information for FNAL in'] )

    def test_showCountries( self ) :
        self.doPage( endPoint='main.showCountries', userName='user1',
                     isIn=['iCMS - EPR Country Overview in'] )

    def test_showCountry( self ) :
        self.doPage( endPoint='main.showCountry', userName='user1', name='USA',
                     isIn=['iCMS - EPR Country Information for USA in %s' % self.selYear] )

    def test_showCountryPost( self ) :
        self.doPagePost( endPoint='main.showCountry', userName='user1', name = 'USA',
                         isIn=[ 'iCMS - EPR Country Information for USA in %s' % self.selYear ] )

    # editing the project is only allowed for admins:

    def test_project_user(self):
        self.doPage( endPoint='main.project', userName='user1', id=1,
                     isIn=[ 'Project pr1' ],
                     isNotIn= ['Short name'] )

    def test_project_PM( self ) :
        self.doPage( endPoint='main.project', userName='p1m1', id=1,
                     isIn=[ 'Project pr1' ],
                     isNotIn= ['Short name'] )

    def test_project_admin(self):
        self.doPage( endPoint='main.project', userName='pfeiffer', id=1, adminOK=True,
                     isIn=[ 'Project pr1', 'Short name' ] )

    def test_projectPost_admin(self):
        self.doPagePost( endPoint='main.project', userName='pfeiffer', id=3, adminOK=True,
                         data = { 'name' : 'pr3', 'description' : 'new desc for p3' },
                         isIn=[ 'Project pr3', 'Short name',
                                'class="alert alert-warning"',
                                'Project info successfully updated.' ] )

    # editing an activity is only allowed for admins and PMs:
    # ... and is done in the manage/ blueprint !

    def test_showCountryGet(self ) :
        # no country specified, so it will fall back to the user's one ...
        self.doPage( endPoint='main.showCountry', userName='user1',
                         isIn=[ 'iCMS - EPR Country Information for SWITZERLAND in %s' % self.selYear ] )

    def test_showManagedPledgesFail( self ):
        self.doPage( endPoint='main.showManagedPledges', userName='user1',
                     isIn=[ 'no managed tasks found for user1' ] )

    def test_showManagedPledges( self ):
        self.doPage( endPoint='main.showManagedPledges', userName='t1m1',
                     isIn=[ 'iCMS - EPR Managed items for t1, m1' ] )

    def test_showTaskFail( self ):
        self.doPage( endPoint='main.showTask',
                     userName='user1',
                     code='nonExistingTaskCode', # should redirect to main page, so check this:
                     isIn=['iCMS - EPR Pledge Overview'] )

    def test_showTask( self ):
        self.doPage( endPoint='main.showTask',
                     userName='user1',
                     code='t1',
                     isIn=['iCMS - EPR Task Overview',
                           'Task ta1 in %d' % self.selYear,
                           'Activity: ac1',
                           'Project:  pr1',
                           'Managers:',
                           'p1m1@example.com,p1m2@example.com',
                           'Institute for Pledge',
                           ],
                     # showPage=True
                    )

    def test_getMailAddrsForPledgees( self ):
        data = { 'year' : self.selYear,
                 'plCodes' : json.dumps( ['1+5+1', '2+5+1', '1+6+2'] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForPledgees', userName='user1',
                         data= data,
                          ) )

        log.info('===>>> found mailaddrs: %s' % res['mailAddrs'])

        self.assertEqual( 3, len(res) )
        self.assertEqual( [], res['ccList'] )
        self.assertIn( "user2@example.com", res['mailAddrs'] ) # note: user1 exists at CERN ...

    def test_getMailAddrsForPledgeesNoList( self ):
        data = { 'year' : self.selYear,
                 'plCodes' : json.dumps( [] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForPledgees', userName='user1',
                                            data= data,
                                            isIn=['No pledges found for :']
                          ) )
        self.assertEqual( 3, len(res) )
        self.assertEqual( [], res['ccList'] )
        self.assertEqual( [], res['mailAddrs'] )
        self.assertEqual( u'No pledges found for : []', res['msg'] )

    def test_getMailAddrsForPledgeesFail1( self ):
        data = { 'year' : self.selYear,
                 # 'plCodes' : json.dumps( [] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForPledgees', userName='user1',
                                            data= data,
                          ) )
        self.assertEqual( u'No pledge codes found in request ', res['msg'] )

    def test_getMailAddrsForPledgeesFail2( self ):
        data = { 'year' : self.selYear,
                 'plCodes' : json.dumps( ['4242+1717+7777'] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForPledgees', userName='user1',
                                            data= data,
                                            expectedRetCode=404,
                                            isIn=['No pledges found for :']
                          ) )

    def test_getMailAddrsForInstLeaders( self ):
        data = { 'year' : self.selYear,
                 'instCodes' : json.dumps( ['CERN', 'FNAL', 'ETHZ'] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForInstLeaders', userName='user1',
                                           data= data,
                                           # showPage=True,
                                         ) )

        self.assertEqual( 3, len(res) )
        self.assertEqual( [], res['ccList'] )
        self.assertEqual( 'found 3 mails for 3 institutes ', res['msg'] )
        self.assertListEqual ( sorted(['cernLdr@example.com', 'ethLdr@example.com', 'fnalLdr@example.com']), sorted(res['mailAddrs']) )

    def test_getMailAddrsForInstLeadersFail1( self ):
        data = { 'year' : self.selYear,
                 # 'instCodes' : json.dumps( ['CERN', 'FNAL', 'ETHZ'] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForInstLeaders', userName='user1',
                                           data= data,
                                           # showPage=True,
                                         ) )

        self.assertEqual( 3, len(res) )
        self.assertEqual( [], res['ccList'] )
        self.assertEqual( [], res['mailAddrs'] )

    def test_getMailAddrsForInstLeadersFail2( self ):
        data = { 'year' : self.selYear,
                 # 'instCodes' : json.dumps( ['CERN', 'FNAL', 'ETHZ'] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForInstLeaders', userName='user1',
                                           data= data,
                                           # showPage=True,
                                         ) )

        self.assertEqual( [], res['ccList'] )
        self.assertEqual( [], res['mailAddrs'] )
        self.assertEqual( u'No institute codes found in request ', res['msg'] )

    def test_getMailAddrsForInstLeadersFail3( self ):
        data = { 'year' : self.selYear,
                 'instCodes' : json.dumps( [] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForInstLeaders', userName='user1',
                                           data= data,
                                           # showPage=True,
                                         ) )

        self.assertEqual( u'No institute codes  found for : []', res['msg'] )

    def test_getMailAddrsForInstLeadersFail4( self ):
        data = { 'year' : self.selYear,
                 'instCodes' : json.dumps( ['fooInst'] )
               }
        res = json.loads( self.doPagePost( endPoint='main.getMailAddrsForInstLeaders', userName='user1',
                                           data= data,
                                           expectedRetCode=403,
                                           # showPage=True,
                                         ) )

        self.assertEqual( u"No pledges found for : ['fooInst']", res['msg'] )

    def test_showProjectNewYear(self):
        yesterYear = self.selYear-1

        self.doPage( endPoint='main.showProject', userName='user1', id=1,
                     isInRe=[re.compile('iCMS - EPR Pledge Overview\s+for pr1\s+in %s' % self.selYear)] )
        self.doPage( endPoint='main.setYear', userName='user1', selYear=yesterYear,
                     isIn=[ 'iCMS - EPR Pledge Overview\n for user, one' ] )
        self.doPage( endPoint='main.showProject', userName='user1', id=1, # showPage=True,
                     isInRe=[re.compile('iCMS - EPR Pledge Overview\s+for pr1\s+in %s' % yesterYear)] )

    def test_loginShib( self ):
        self.doPage( endPoint='auth.login', userName='pfeiffer', adminOK=True,
                     headers = { 'User' : 'andreas.pfeiffer@cern.ch' },
                     isIn=['iCMS - EPR Pledge Overview'] )

    def test_logout( self ):
        self.doPage( endPoint='auth.logout', userName='user1',
                     data = None,
                     isInAlert=['You have been logged out.'] )

    def test_loginAs( self ):
        self.doPagePost( endPoint='auth.loginAs', userName='pfeiffer', adminOK=True,
                         data = { 'userSel' : 'foo - (CERNid: 99002)' }, # user3
                         isIn=['iCMS - EPR Pledge Overview\n for user, three'] )

    def test_showFavourites(self):
        self.doPage( endPoint='main.showFavourites', userName='user1',
                     data = None,
                     # showPage=True,
                     isIn=['Favourites'] )

    def test_showPledge(self):
        self.doPage( endPoint='main.showPledge', userName='user1', code='2:5:1',
                     data = None,
                     # showPage=True,
                     isIn=['iCMS - EPR Pledge Overview', 'Add as favorite',
                           '<tr> <td width="20%"> <b> User </b> </td> <td width="40%"> user, one </td> </tr>',
                           '<tr> <td width="20%"> <b> Work-time pledged </b> </td> <td width="40%"> 1.2 </td> </tr>'] )

    def test_showPledgeFail(self):
        self.doPage( endPoint='main.showPledge', userName='user1', code='1:1:1',
                     data = None,
                     # showPage=True,
                     isInRe=[ re.compile( r'iCMS - EPR Pledge Overview\s+for user, one\s+in %s' % self.selYear) ],
                     isInAlert=['Could not find Pledge for 1+1+1 ']
                     )

    def test_findProjectInYear(self):

        # find last years project in last year:
        yesterYear = self.selYear-1
        p1_lastYear = db.session.query(Project).filter_by(code='p1a', year=self.selYear-1).one()
        self.doPage( endPoint='main.setYear', userName='user1', selYear=yesterYear,
                     isIn=[ 'iCMS - EPR Pledge Overview\n for user, one' ] )
        self.doPage( endPoint='main.showProject', userName='user1', id=p1_lastYear.id, # showPage=True,
                     isInRe=[re.compile('iCMS - EPR Pledge Overview\s+for pr1\s+in %s' % yesterYear)] )

        # back to this year, but keep old project ID
        self.doPage( endPoint='main.setYear', userName='user1', selYear=self.selYear,
                     isIn=[ 'iCMS - EPR Pledge Overview\n for user, one' ] )
        self.doPage( endPoint='main.showProject', userName='user1', id=p1_lastYear.id, # showPage=True,
                     isInRe=[re.compile('iCMS - EPR Pledge Overview\s+for pr1\s+in %s' % self.selYear)] )

    def test_findProjectInYearFail1(self):

        # no project, redirects to user home page
        self.doPage( endPoint='main.showProject', userName='user1', id=9999, # showPage=True,
                     isInRe = [ re.compile('iCMS - EPR Pledge Overview\s+for user, one\s+in %d' % self.selYear) ] )

    def test_findProjectInYearFail2(self):

        # try to find a project "pr1" which exists only in <testTear>, should fail
        yesterYear = self.selYear-1
        p2 = db.session.query(Project).filter_by(code='p2', year=self.selYear).one()
        self.doPage( endPoint='main.setYear', userName='user1', selYear=yesterYear,
                     isInRe = [ re.compile('iCMS - EPR Pledge Overview\s+for user, one' ) ] )
        # getting the non-existing project now redirects to the user's home page in the requested year
        self.doPage( endPoint='main.showProject', userName='user1', id=p2.id, # showPage=True,
                     isInAlert=['ERROR: could not find a project with same name (as id %s) for %s in DB to start with ... giving up.' % (p2.id, yesterYear)],
                     isInRe = [ re.compile('iCMS - EPR Pledge Overview\s+for user, one\s+in %d' % yesterYear) ] )


    def test_showProjStatus( self ):

        self.doPage( endPoint='main.showProjStats', userName='user1', id=9999, # showPage=True,
                     isInRe = [ re.compile('iCMS - EPR Project Statistics Overview in %d' % self.selYear) ] )
