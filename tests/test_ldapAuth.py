#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import unittest
import os

from app import create_app

import app.ldapAuth as la

class LDAPAuthTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    def test_getPrimUserByDN(self):

        dn = 'CN=%s,OU=Users,OU=Organic Units,DC=cern,DC=ch' % 'pfeiffer'
        response = la.getPrimUserByDN(dn)
        self.assertEqual( [b'6635'], response['uidNumber'] )

        dn = 'CN=%s,OU=Users,OU=Organic Units,DC=cern,DC=ch' % 'aptstcms'
        response = la.getPrimUserByDN(dn)
        self.assertIsNotNone( response )
        self.assertEqual( [b'6635'], response['uidNumber'] )

        dn = 'CN=%s,OU=Users,OU=Organic Units,DC=cern,DC=ch' % 'aptstFOO'
        response = la.getPrimUserByDN( dn )
        self.assertIsNone( response )

    def test_getPrimUserByHrId(self):

        hrId = '422405'
        response = la.getPrimUserByHrId(hrId)
        self.assertEqual( [b'6635'], response['uidNumber'] )

    def test_authenticate(self):
        import netrc
        netRcFile = netrc.netrc()
        (login, account, pwd) = netRcFile.authenticators( 'ldap.cern.ch' )

        response = la.authenticate(login, pwd)
        self.assertTrue( response )

    def test_getLogin(self):

        hrId = '422405'
        response = la.getLogin(hrId)
        self.assertEqual('pfeiffer', response)

    def test_getEmail(self):

        login = 'pfeiffer'
        response = la.getUserEmail(login)
        self.assertEqual('Andreas.Pfeiffer@cern.ch', response)

    def test_getUserFromEmail(self):

        email = 'andreas.pfeiffer@cern.ch'
        response = la.getUserFromEmail(email)
        self.assertEqual('pfeiffer', response)

    def test_getUserFromEmailFail(self):

        email = 'foo00.baaar@cern.ch'
        response = la.getUserFromEmail(email)
        self.assertFalse( response)

    def test_userInGroup(self):

        response = la.userInGroup('pfeiffer', 'zh')
        self.assertTrue( response )

        response = la.userInGroup('pfeiffer', 'zp')
        self.assertFalse( response )

        response = la.userInGroup('pfeiffer', 'foo')
        self.assertFalse( response )

        response = la.userInGroup( 'pfeifferFoo', 'zh' )
        self.assertFalse( response )

    def test_ldapMain(self):

        la.test()
