
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
from app.main.JsonDiffer import *

from .BaseTestClass import *

log = logging.getLogger( "Testing.test_JsonDiffer" )
log.setLevel(logging.DEBUG)

class JsonDifferTestCase(BaseTestCase):

    def setUp(self):
        BaseTestCase.setUp( self )

        data1 = { 'json1' : 'val1', 'item1' : (1,), 'bool' : True ,
                  'aList': [1,2,3],
                  'a1List': [1,2,3],
                  'bList' : [ 1, 2, 3],
                  'aTup': (1,2,3),
                  'dict1' : { "a":1, 'b':False } }
        data2 = { 'json1' : 'val1', 'item2' : 2, 'bool' : False,
                  'aList': [1,2,3],
                  'a1List': [1,0,3],
                  'bList': [0,1,2,3,4],
                  'aTup': [1,2,3],
                  'dict1' : { "a":1, 'b':False, 'd':42 }  }

        with open( '/tmp/foo1.json', 'w' ) as jFile : json.dump( data1, jFile )
        with open( '/tmp/foo2.json', 'w' ) as jFile : json.dump( data2, jFile )
        with open( '/tmp/foo1a.json', 'w') as jFile : json.dump( data1, jFile )

        with open( '/tmp/fooBad.json', 'w') as jFile : jFile.write( "foo" )

        self.d1 = data1
        self.d2 = data2

    def tearDown(self):
        import os
        os.unlink( '/tmp/foo1.json' )
        os.unlink( '/tmp/foo1a.json' )
        os.unlink( '/tmp/foo2.json' )
        os.unlink( '/tmp/fooBad.json' )

    def test_differ(self):
        self.assertTrue( is_scalar(0) )

    def test_mainFail( self ) :

        res = main(['self'])
        self.assertEqual( -2, res )

    def test_mainOut( self ) :
        self.assertEqual( 1, main( [ 'self', '-o=tmpDiff', '-H', '/tmp/foo1.json', '/tmp/foo2.json' ] ) )
        self.assertEqual( 1, main( [ 'self', '-o=tmpDiff', '-H', '/tmp/foo2.json', '/tmp/foo1.json' ] ) )

    def test_main( self ) :
        self.assertEqual( 1, main(['self', '-o=tmpDiff', '/tmp/foo1.json', '/tmp/foo2.json']) )

    def test_mainIdentical( self ) :
        self.assertEqual( 0, main(['self', '-o=tmpDiff', '/tmp/foo1.json', '/tmp/foo1a.json']) )

    def test_comparator(self):
        cmp = Comparator()
        cmp.compare_dicts(self.d1, self.d2)

        try:
            cmp0 = Comparator( fn1 = open( '/tmp/foo1.json' ), fn2 = open( '/tmp/fooBad.json' ) )
        except BadJSONError:
            pass
        except:
            raise

        try:
            cmp1 = Comparator( fn1 = open( '/tmp/fooBad.json' ), fn2 = open( '/tmp/foo1.json' ) )
        except BadJSONError:
            pass
        except:
            raise

        # just to make sure we really got here ... :-D
        res = True
        self.assertTrue( res )
