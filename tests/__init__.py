
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from unittest import TestSuite

from .test_basics import BasicsTestCase
from .test_client import ClientTestCase

test_cases = ( BasicsTestCase, ClientTestCase )

def load_tests(loader, tests, pattern):
    print("load tests called ... ")
    suite = TestSuite()
    for test_class in test_cases:
        tests = loader.loadTestsFromTestCase(test_class)
        suite.addTests(tests)
    return suite
