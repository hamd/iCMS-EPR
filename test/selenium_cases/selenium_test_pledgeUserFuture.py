
import time

from .SeleniumTestBase import SeleniumTestCase, loginUser

from selenium.webdriver.common.action_chains import ActionChains

from .pledges import createPledge, updatePledge, rejectPledge

class PledgeUserFutureTestCase(SeleniumTestCase):

    def setUp(self):
        super(PledgeUserFutureTestCase, self).setUp()
        self.id = 'plU2020'

        self.selYearFuture = self.selYear + 3

        self.username = 'pfeiffer'
        loginUser( username = self.username, theTest=self )

        self.logger.info("%s> %s logged in ... " % (self.id, self.username) )

    def testPledgeUserFuture( self ) :

        # loginUser('user1', self)

        self.logger.info( 'going to create pledge for user %s in %s' % (self.username, self.selYearFuture) )

        # first, set the year to the selected future year.
        # I did not find a way to do this from the nav menu with Selenium (the drop-down menu was found but did not open)
        self.go_to('/setYear/%s' % self.selYearFuture)

        self.debugLogWithScreenShot('testPledgeUserFuture' )

        createPledge( self, username=self.username, name='Pfeiffer, Andreas', year=self.selYearFuture, instMgr=True )

        self.debugLogWithScreenShot('testPledgeUserFuture-afterCreate')

        self.assertIn( 'New pledges are not allowed for %s' % self.selYearFuture, self.driver.page_source )

        # for the future :-) - if you need this, don't forget to change the corresponding line (~845) in main/views.py:
        # self.assertIn( 'A new pledge was successfully created for user pfeiffer', self.driver.page_source )

    def testNavBar2020(self):

        self.debugLogWithScreenShot('navBar%s-0' % self.selYearFuture)

        selTask = self.driver.find_element_by_xpath( '//a[contains(text(),"Tasks")]' )
        ActionChains( self.driver ).move_to_element( selTask ).click( selTask ).perform( )

        tasksPr1 = self.driver.find_elements_by_xpath( '//a[contains(@href,"showProjectTasks")]' )
        self.assertEqual( len(tasksPr1), 6 )

        # wait = WebDriverWait( self.driver, 10 )
        # wait.until( EC.visibility_of( (By.PARTIAL_LINK_TEXT, 'Tasks for pr1') ) )
        # ActionChains( self.driver ).move_to_element( confirmSelUserForm ).click( confirmSelUser ).perform( )

        self.debugLogWithScreenShot('navBar%s-1' % self.selYearFuture)

        # now the same after setting the year to 2020.
        # I did not find a way to do this from the nav menu with Selenium (the drop-down menu was found but did not open)
        self.go_to( '/setYear/%s' % self.selYearFuture)

        selTask = self.driver.find_element_by_xpath( '//a[contains(text(),"Tasks")]' )
        ActionChains( self.driver ).move_to_element( selTask ).click( selTask ).perform( )

        self.debugLogWithScreenShot( 'navBar%s-2' % self.selYearFuture )

        time.sleep(2) # wait a bit for element to show ...
        tasksPr1a = self.driver.find_elements_by_xpath( '//a[contains(@href,"showProjectTasks")]' )
        self.logger.info( '[navBar] found %s tasks in %s: %s ' % (len(tasksPr1a), self.selYearFuture, [x.get_attribute('href') for x in tasksPr1a] ) )
        self.assertEqual( len(tasksPr1a), 5 )
