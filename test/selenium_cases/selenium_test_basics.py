
from .SeleniumTestBase import SeleniumTestCase, debugLog

class BasicThingsTestCase(SeleniumTestCase):

    def setUp(self):
        super(BasicThingsTestCase, self).setUp()
        self.id = 'basicThings'


    def test_render_main_page(self):
        dvr = self.driver
        self.logger.info("rendering main page ...")
        self.go_to('/')
        self.assertIn('iCMS-EPR - Login', dvr.title)
        print( dvr.title )
        # submit = dvr.find_element_by_id('submit')

    def testLoginFail(self):
        self.go_to('/auth/login')
        self.assertIn('iCMS-EPR - Login', self.driver.title)
        self.driver.find_element_by_id('username').send_keys("iCMStester")
        self.driver.find_element_by_id('password').send_keys("iCMStesterFail")
        self.driver.find_element_by_id('submit').click()

        alertWarn = self.driver.find_elements_by_class_name('alert-warning')
        self.assertIn('User iCMStester not found in DB', alertWarn[0].text)

    def testLogin(self):
        self.go_to('/auth/login')
        self.assertIn('iCMS-EPR - Login', self.driver.title)
        self.driver.find_element_by_id('username').send_keys(self.auth[0])
        self.driver.find_element_by_id('password').send_keys(self.auth[1])
        self.driver.find_element_by_id('submit').click()

        debugLog( 'login-0', self.driver.page_source.encode('utf-8') )

        # this needs to be updated along the "news" section. At least there is one line
        # with the warning that it's not the production version ...
        alertWarn = self.driver.find_elements_by_class_name('alert-warning')
        if len(alertWarn) > 0:
            logFileName = '%s-test-login-alertWarn' % self.id
            debugLog( logFileName, self.driver.page_source.encode( 'utf-8' ) )
        self.assertListEqual( alertWarn , [])

        self.assertIn( 'iCMS - EPR Pledge Overview for Pfeiffer, Andreas', self.driver.title )
        debugLog( 'login', self.driver.page_source.encode('utf-8') )

