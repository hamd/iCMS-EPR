

from .SeleniumTestBase import SeleniumTestCase, loginUser, clearAndSend

selYear = 2015

class PledgeUserPrevYearsTestCaseFail(SeleniumTestCase):

    def setUp(self):
        super(PledgeUserPrevYearsTestCaseFail, self).setUp()
        self.id = 'plU2015Fail'

        self.username = 'user4' # user 4 is active in lastYear
        loginUser( username = self.username, theTest=self )

        self.logger.info("%s> %s logged in ... " % (self.id, self.username) )

    def testPledgeUserLastYearFail( self ) :

        selYear = self.selYear-1
        # loginUser('user1', self)

        self.logger.info( 'going to create pledge for user %s in %s' % (self.username, selYear) )

        # first, set the year to last year.
        # I did not find a way to do this from the nav menu with Selenium (the drop-down menu was found but did not open)
        self.go_to('/setYear/%s' % selYear)

        self.debugLogWithScreenShot('testPledgeUser%s' % selYear)

        self.go_to('/newPledge/t4a')

        self.debugLogWithScreenShot('testPledgeUser%s-afterCreate' % selYear)

        self.assertIn( 'New pledges are not allowed for %s' % selYear, self.driver.page_source )

        # for the future :-) - if you need this, don't forget to change the corresponding line (~845) in main/views.py:
        # self.assertIn( 'A new pledge was successfully created for user pfeiffer', self.driver.page_source )


class PledgeUserPrevYearsTestCase(SeleniumTestCase):

    def setUp(self):
        super(PledgeUserPrevYearsTestCase, self).setUp()
        self.id = 'plU2015OK'

        self.username = 'p1m1' # manager for a4 in lastYear
        loginUser( username = self.username, theTest=self )

        self.logger.info("%s> %s logged in ... " % (self.id, self.username) )

    def testPledgeUserLastYearPMOK( self ) :

        selYear = self.selYear-1
        # loginUser('user1', self)

        self.logger.info( 'going to create pledge for user %s in %s' % (self.username, selYear) )

        # first, set the year to last year.
        # I did not find a way to do this from the nav menu with Selenium (the drop-down menu was found but did not open)
        self.go_to('/setYear/%s' % selYear)

        self.debugLogWithScreenShot('testPledgeUser%s' % selYear)

        self.go_to('/newPledge/t4a')

        self.debugLogWithScreenShot('testPledgeUser%s-afterCreate' % selYear)

        self.assertNotIn( 'New pledges are not allowed for %d' % selYear, self.driver.page_source )
        self.assertIn( 'Creating New Pledge in %d' % selYear, self.driver.page_source )

