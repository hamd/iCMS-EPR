import unittest
import time
import netrc
import os

from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.proxy import Proxy, ProxyType
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common import exceptions

# from selenium.webdriver.firefox.options import Options
# from selenium.webdriver.chrome.options import Options

import logging
from datetime import date

from testutils import URL_TEST_PORT

# import here so we can use it in all tests
from app.main.Helpers import getRequiredWork, getPledgeYear

myProxy = "127.0.0.1:1961"
proxy = Proxy( {
            'proxyType'  : ProxyType.MANUAL,
            'httpProxy'  : myProxy,
            'httpsProxy' : myProxy,
            'ftpProxy'   : myProxy,
            'sslProxy'   : myProxy,
            'noProxy'    : ''  # set this value as desired
        } )


def getEprAssets():
    assets = {
              "name": "epr_year_params",
              "application": "epr",
              "data": {
                "pledgeYear": 2019,
                "reqWork": { "2015": 4.5,
                  "2016": 3.0,
                  "2017": 3.0,
                  "2018": 4.0,
                  "2019": 4.0,
                  "2020": 4.0
                },
                "cspPerAuthor": {
                  "2015": 0.0,
                  "2016": 8.1,
                  "2017": 8.9,
                  "2018": 9.572632623,
                  "2019": 9.572632623, # 3.4,
                  "2020": 99999.0
                }
              }
            }
    return assets['data']

class SeleniumTestCase(unittest.TestCase):

    # todo: move these parameters to some settings or sth like that
    test_host = '127.0.0.1'
    test_port = URL_TEST_PORT

    def setUp(self):

        logging.basicConfig( format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG )
        self.logger = logging.getLogger("SeleniumTesting")
        self.logger.setLevel( logging.DEBUG )

        self.logger.info("starting up ... %s " % type(self).__name__ )

        # self.driver = webdriver.PhantomJS()

        # profile = webdriver.FirefoxProfile( '/Users/ap/Library/Application Support/Firefox/Profiles/fbz41a3m.selenium' )
        # print( "profile from: ", profile.profile_dir )
        # self.driver = webdriver.Firefox( firefox_binary=webdriver.firefox.firefox_binary.FirefoxBinary(log_file=open('/tmp/firefox.log','w')),
        #                                  firefox_profile=profile, timeout=10 )

        self.driver = webdriver.Firefox( timeout=15 )

        # options = Options()
        # options.headless = True  # older webdriver versions
        # # options.set_headless( True )  # newer webdriver versions
        # options.add_argument( '--no-sandbox' )
        # self.logger.info("option no-sandbox for chromium set ... ")
        # self.driver = webdriver.Chrome( options=options )

        # options = webdriver.ChromeOptions()
        # options.add_argument( 'headless' )
        #
        # self.driver = webdriver.Chrome( chrome_options=options )


        self.driver.set_window_size(1920, 1080)
        self.driver.set_page_load_timeout(30) # DB setup takes O(15 sec)

        # todo: docs say not to mix implicit and explicit
        self.driver.implicitly_wait(10)

        self.selYear = int(getEprAssets()['pledgeYear'])
        self.monthsPerAuthor = getEprAssets()['reqWork'][str(self.selYear)]

        try:
            self.auth = ('pfeiffer', netrc.netrc( ).authenticators( 'seltest' )[ 2 ])
        except IOError:
            self.auth = ['pfeiffer', 'foo']

        self.test_host = '127.0.0.1'
        self.test_port = URL_TEST_PORT

        self.go_to(path='/restore_db/full') # make sure we also create an initial set of pledges
        self.logger.info( 'Post-setUp nap' )

    def tearDown(self):
        # self.driver.refresh()
        self.go_to( path='/rollback_db' )
        self.driver.quit()

    def screen_shot(self, title):
        self.driver.get_screenshot_as_file('/tmp/%s.png' % title)

    def debugLogWithScreenShot(self, title):

        # do this only when the env-var is set, so we don't waste a lot of time in automated debugging ...
        if os.environ.get('DO_DBG_SCREENSHOTS', False):
            logFileName = '%s-test-%s' % (self.id, title)
            self.driver.save_screenshot( '../logs/%s.png' % logFileName )
            debugLog( logFileName, self.driver.page_source.encode( 'utf-8' ) )

    def findInAlerts( self, what) :

        found = False
        for alertType in ['alert-warning', 'alert-danger']:
            alertItems = self.driver.find_elements_by_class_name( alertType )
            self.logger.debug('%s elements for %s ' % (len(alertItems), alertType) )
            for item in alertItems :
                uItem = item.text.replace('\n', ' ')
                self.logger.debug( "[%s]%s> found: '%s' " % ( self.id, alertType, uItem ) )
                if what in uItem :
                    found = True

        return found

    def go_to(self, path='', driver=None):
        if driver is None:
            driver = self.driver

        url = 'http://%s:%d/%s' % (self.test_host, self.test_port, path if not path.startswith( '/' ) else path[ 1 : ])
        self.logger.info('going to %s ' % url)
        return driver.get(url)

    def _map_table_rows(self, table_element=None, key_index=None):
        """
        Converts each table row into a map with keys taken from headers.
        If key_index is specified, the result will be a map, otherwise a list of maps
        """

        if table_element is None:
            table_element = self.driver.find_element_by_tag_name('table')

        result = {} if key_index is not None else []
        thead = table_element.find_element_by_tag_name('thead')
        tbody = table_element.find_element_by_tag_name('tbody')
        keys = [th.text for th in thead.find_elements_by_tag_name('th')]
        for tr in tbody.find_elements_by_tag_name('tr'):
            values = [td.text for td in tr.find_elements_by_tag_name('td')]
            mapped = {k:v for k,v in zip(keys, values)}
            if key_index is not None:
                result[values[key_index]] = mapped
            else:
                result.append(mapped)
        return result

    def _find_row_by_partial_text(self, text, parent_element=None):
        if parent_element is None:
            parent_element = self.driver
        for row in parent_element.find_elements_by_tag_name('tr'):
            self.logger.info( 'Row text: %s' % row.text )
            if text in row.text:
                self.logger.info( '%s found in %s' % (text, row.text) )
                return row
        return None

    def _wait_until_text_in_id(self, text, id):
        wait = WebDriverWait(driver=self.driver, timeout=10, poll_frequency=1)
        element = wait.until(EC.text_to_be_present_in_element((By.ID, id), text))
        self.logger.debug('Waiting for {0} in element #{1} yielded {2}'.format(text, id, element))

    def _fill_in_date_field(self, field, value):
        if isinstance(value, str):
            field.send_keys(value)
        elif isinstance(value, date):
            field.send_keys('%04d-%02d-%02d' % (value.year, value.month, value.day))

#
# some helper functions
#

def debugLog( id, page ) :

    # do this only when the env-var is set, so we don't waste a lot of time in automated debugging ...
    if os.environ.get( 'DO_DBG_SCREENSHOTS', False ) :

        if not os.path.exists('../logs/') : os.makedirs('../logs/')

        with open( '../logs/debugLog-%s.html' % id, 'w' ) as dlf :
            dlf.write( str( page ) )

def debugLogWithScreenShot( theTest, title ) :
    # do this only when the env-var is set, so we don't waste a lot of time in automated debugging ...
    if os.environ.get( 'DO_DBG_SCREENSHOTS', False ) :
        logFileName = '%s-test-%s' % (theTest.id, title)
        theTest.driver.save_screenshot( '../logs/%s.png' % logFileName )
        debugLog( logFileName, (theTest.driver.page_source).encode('utf-8') )

def loginUser( username, theTest ) :

    try:
        authInfo = (username, netrc.netrc().authenticators('seltest')[2])
    except IOError :
        authInfo = [ username, 'foo' ]

    theTest.go_to( '/auth/login' )
    theTest.assertIn( 'iCMS-EPR - Login', theTest.driver.title )
    theTest.driver.find_element_by_id( 'username' ).send_keys( authInfo[ 0 ] )
    theTest.driver.find_element_by_id( 'password' ).send_keys( authInfo[ 1 ] )
    theTest.driver.find_element_by_id( 'submit' ).click( )

    # make sure we're working on the test DB
    theTest.assertIn( 'This is not the production version, all changes will be discarded', theTest.driver.page_source )
    alertWarn = theTest.driver.find_elements_by_class_name( 'alert-warning' )
    if alertWarn != []:
        logging.warning( "++> found unexpected warning: %s " % alertWarn[0].text )
        theTest.debugLogWithScreenShot('plU-test-setup')
    theTest.assertListEqual( alertWarn, [ ] )

    news = theTest.driver.find_element_by_class_name( 'news' )
    theTest.assertIn( 'is not the production version', news.text )

    theTest.driver.save_screenshot( '/data/logs/ff-plU-test-setup.png' )
    debugLog( 'plU-test-setup', theTest.driver.page_source.encode( 'utf-8' ) )

def clearAndSend( theTest, elementId, value ) :
    # this is now an auto-completing search field:
    userSel = theTest.driver.find_element_by_id( elementId )
    act = ActionChains( theTest.driver )
    act.move_to_element( userSel ).double_click( userSel ).send_keys( value ).perform()

