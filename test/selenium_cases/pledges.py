
import time, os

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.action_chains import ActionChains
from selenium.common import exceptions

from .SeleniumTestBase import debugLog, debugLogWithScreenShot, clearAndSend, getPledgeYear

def waitAbit(theTest, howLong=2):
    # theTest.driver.implicit_wait( howLong )
    return

def createPledge( theTest, username, name, year=None, instMgr=False ) :

    if year is None: year = getPledgeYear( 2019 )
    theTest.logger.debug( "found year: %d" % year)

    # make sure we're working on the test DB
    theTest.assertIn( 'This is not the production version, all changes will be discarded', theTest.driver.page_source )

    newPl = theTest.driver.find_element_by_xpath( "//a[@href='/makeNewPledge']" )
    theTest.driver.get( newPl.get_attribute( 'href' ) )

    wait = WebDriverWait( theTest.driver, 10 )
    wait.until( EC.title_contains( 'iCMS - EPR Select Task' ) )

    debugLogWithScreenShot( theTest, 'Pledge-selectTask-p' )

    selProj = Select( theTest.driver.find_element_by_id( 'projects' ) )
    projs = '\n'.join( [ o.text for o in selProj.options ] )
    theTest.logger.debug( "found projects: \n %s" % projs )
    selProj.select_by_visible_text( 'pr1' )
    waitAbit( theTest )

    debugLogWithScreenShot( theTest, 'Pledge-selectTask-a' )

    selAct = Select( theTest.driver.find_element_by_id( 'activities' ) )
    acts = '\n'.join( [ o.text for o in selAct.options ] )
    theTest.logger.debug( "found activities: \n %s" % acts )
    selAct.select_by_visible_text( 'ac1' )
    waitAbit( theTest )

    debugLogWithScreenShot( theTest, 'Pledge-selectTask-t' )

    selTask = Select( theTest.driver.find_element_by_id( 'tasks' ) )
    tasks = '\n'.join( [ o.text for o in selTask.options ] )
    theTest.logger.debug( "found tasks: \n %s" % tasks )
    selTask.select_by_visible_text( 'ta1' )
    waitAbit( theTest )

    theTest.driver.find_element_by_id( 'selectButton' ).click( )

    debugLogWithScreenShot( theTest, 'Pledge-taskSelected' )

    if 'New pledges are not allowed for %s' % year in theTest.driver.page_source:
        return -1

    theTest.logger.debug( 'task selected, going to create pledge for user %s (as user %s) ... ' % (username, theTest.username) )

    wait.until( EC.title_contains( 'iCMS - EPR New Pledge' ) )

    debugLogWithScreenShot( theTest, 'Pledge-create-0' )

    theTest.assertIn( 'iCMS - EPR New Pledge', theTest.driver.title )

    # keep this for pledge-on-behalf
    # # this is now an auto-completing search field:
    # userSel = theTest.driver.find_element_by_id( 'userSel' )
    # userSel.clear( )
    # userSel.send_keys( username )
    # time.sleep( 1 )
    # userSel.send_keys( Keys.TAB )
    # userSel.click( )

    waitAbit( theTest, 1 )

    # make sure we're working on the test DB
    theTest.assertIn( 'This is not the production version, all changes will be discarded', theTest.driver.page_source )

    if instMgr:
        # confirm the pledge creation (needed as admin !)
        confirmButtonText = "Yes, I really want to create this pledge for %d" % year
        confirmButton = theTest.driver.find_element_by_xpath( "//button/span[contains(text(), '%s') and @class='ui-button-text']" % confirmButtonText )
        wait = WebDriverWait( theTest.driver, 10 )
        wait.until( EC.element_to_be_clickable( (By.XPATH, "//button/span[contains(text(), '%s') and @class='ui-button-text']" % confirmButtonText) ) )

        confirmButton.click()

    debugLogWithScreenShot( theTest, 'Pledge-create-1a' )

    clearAndSend(theTest, 'workTimePld', '1')
    clearAndSend(theTest, 'workedSoFar', '1')

    debugLogWithScreenShot( theTest, 'Pledge-create-1b' )

    submitBtn = theTest.driver.find_element_by_xpath( "//form[@name='newPledge']" )
    wait = WebDriverWait( theTest.driver, 10 )
    wait.until( EC.element_to_be_clickable( (By.XPATH, "//form[@name='newPledge']" ) ) )
    submitBtn.submit( )

    debugLogWithScreenShot( theTest, 'Pledge-create-1c' )

    alertCrit = theTest.driver.find_elements_by_class_name( 'alert-danger' )
    if alertCrit != [] :
        theTest.logger.warning( "++> found (%s) unexpected alert: %s " % (len(alertCrit), alertCrit[0].text) )
        logFileName = '%s-test-create-alertCrit-%s' % (theTest.id, username)
        debugLog( logFileName, theTest.driver.page_source.encode( 'utf-8' ) )
    theTest.assertListEqual( alertCrit, [] )

    # debugLogWithScreenShot( theTest, 'Pledge-create-1c' )
    #
    # if instMgr:
    #     # second confirmation of the pledge creation (needed as admin !)
    #     confirmButtonText = "Yes, I really want to create this pledge for %d" % year
    #     wait = WebDriverWait( theTest.driver, 10 )
    #     wait.until( EC.element_to_be_clickable( (By.XPATH, "//button[contains(text(), '%s') and @class='confirm btn btn-danger']" % confirmButtonText) ) )
    #     confirmButton1 = theTest.driver.find_element_by_xpath( "//button[contains(text(), '%s') and @class='confirm btn btn-danger']" % confirmButtonText )
    #     confirmButton1.click()

    debugLogWithScreenShot( theTest, 'Pledge-create-2' )

    expectedTitle = 'iCMS - EPR Pledge Overview for %s' % name
    # expectedTitle = 'iCMS - EPR New Pledge in %d' % year
    theTest.assertIn( expectedTitle.lower(), theTest.driver.title.lower() )
    alertWarn = theTest.driver.find_elements_by_class_name( 'alert-warning' )

    debugLogWithScreenShot( theTest, 'Pledge-create-3' )

    theTest.assertIn( 'A new pledge was successfully created for user', alertWarn[ 0 ].text )

    theTest.logger.info( 'pledge created for user %s ', theTest.username )

    theTest.assertIn( expectedTitle.lower(), theTest.driver.title.lower() )
    theTest.assertNotIn( '502 Bad Gateway', theTest.driver.page_source )

def updatePledge( theTest, username ) :
    # make sure we're working on the test DB
    theTest.assertIn( 'This is not the production version, all changes will be discarded', theTest.driver.page_source )

    time.sleep(1)

    editPl = None
    for item in theTest.driver.find_elements_by_xpath( '//table[@id="projectTable"]/tbody/tr/td/a' ) :
        link = item.get_attribute( 'href' )
        if '/pledge/' in link.lower( ) :
            editPl = link
            break

    theTest.assertNotEqual(editPl, None)

    theTest.debugLogWithScreenShot( 'updatePledge-0' )

    # make sure we're working on the test DB
    theTest.assertIn( 'This is not the production version, all changes will be discarded', theTest.driver.page_source )
    theTest.logger.info("found link for editing pledge as: '%s'" % editPl)
    theTest.driver.get( editPl )

    clearAndSend( theTest, 'workTimePld', '3.' )
    clearAndSend( theTest, 'workedSoFar', '2.' )
    theTest.driver.find_element_by_xpath( "//input[@type='submit']" ).click( )
    time.sleep(2)
    theTest.assertNotIn( '502 Bad Gateway', theTest.driver.page_source )

    alertCrit = theTest.driver.find_elements_by_class_name( 'alert-danger' )
    theTest.assertListEqual( alertCrit, [ ] )

    alertWarn = theTest.driver.find_elements_by_class_name( 'alert-warning' )
    theTest.assertIn( 'Pledge info successfully updated.', alertWarn[ 0 ].text )

    logFileName = '%s-test-update' % theTest.id
    theTest.driver.save_screenshot( '../logs/%s.png' % logFileName)
    debugLog( logFileName, theTest.driver.page_source.encode( 'utf-8' ) )

    # this part assumes that we only have one pledge !!!
    # check work time pledged got updated (cols count from 1 ... :( )
    theTest.assertEqual( theTest.driver.find_element_by_xpath( '//tbody/tr/td[13]' ).text, '3.00' )

def rejectPledge( theTest, username ) :
    theTest.go_to( '/showMine' )
    waitAbit( theTest, 5 )

    # make sure we're working on the test DB
    theTest.assertIn( 'This is not the production version, all changes will be discarded', theTest.driver.page_source )

    # this part assumes that we only have one pledge !!!
    rejPl = None
    for item in theTest.driver.find_elements_by_xpath( '//tbody/tr/td/a' ) :
        link = item.get_attribute( 'href' )
        if 'reject' in link.lower( ) :
            rejPl = link
            break

    theTest.driver.get( rejPl )
    theTest.assertNotIn( '502 Bad Gateway', theTest.driver.page_source )

    theTest.driver.find_element_by_id( 'reason' ).send_keys( 'remove from testing' )
    theTest.driver.find_element_by_xpath( "//input[@type='submit']" ).click( )
    theTest.assertNotIn( '502 Bad Gateway', theTest.driver.page_source )

    debugLogWithScreenShot( theTest, 'Pledge-reject-0' )

    # this test will fail (or needs more refined selection) if the DebugToolbarExtension is enabled
    theTest.assertListEqual( theTest.driver.find_elements_by_xpath( '//tbody/tr/td/a' ), [ ] )

    theTest.logger.info( 'pledge rejected' )
