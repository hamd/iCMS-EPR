
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os, sys
import logging

from app import create_app, db

import SetupDummies

URL_TEST_PORT=5010

def dump_response(dump_path, rv):
    """
    Takes the response received while testing and saves it as html file somewhere
    :return:
    """
    with open(dump_path, 'w') as f:
        f.write(rv.data)


def get_test_app(keyword='test', reload_all_dbs=False, binds_to_reload=[]):
    """
    A shortcut for invoking the factory and passing all the parameters
    When set to True, reload_all_dbs takes precedence over binds_binds_to_reload
    Otherwise the databases are reloaded one by one as specified by binds_to_reload
    :return:
    """
    theApp = create_app( 'testing' )
    with theApp.app_context( ) :
        dbUrl = str(db.engine.url)
        print( dbUrl )
        if ( not dbUrl.startswith('postgres') or
             (not dbUrl.endswith('TestDB') and not dbUrl.endswith('_test') and not dbUrl.endswith('TestDBNew'))
             ) :
            print( "\nFATAL: will not touch non-test DB %s \n" % (dbUrl[:8]+'...'+dbUrl[-8:]) )
            sys.exit(-1)
        if reload_all_dbs:
            SetupDummies.resetDB( )
            print( "DB reset ..." )
            SetupDummies.setup( pledges=False )
            print( "DB set up (no pledges) ..." )

    return theApp

if __name__ == '__main__':
    logging.debug('Main starts')
    # reload_people_db()
    # populate_epr_db()
