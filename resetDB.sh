
source ./venv-py3/bin/activate
# source /src/test.env

export ICMS_COMMON_ROLE=icms
export ICMS_EPR_ROLE=epr
export ICMS_TOOLKIT_ROLE=toolkit
export ICMS_READER_ROLE=icms_reader
export ICMS_LEGACY_USER=icms_legacy

echo "from /src " ${PWD} " gitlab-ci flag: " ${POSTGRES_ENV_GITLAB_CI}

export POSTGRES_HOST=postgres
export PG_DB_NAME=icms_test
export POSTGRES_HOST_AUTH_METHOD=trust

# use this when running on gitlab-CI
if [ $POSTGRES_ENV_GITLAB_CI ]; then
  echo "Setting up for gitlab-ci ... "
  export PG_USER=runner
else
  export PG_USER=postgres
fi

echo "cleaning up ... "
dropdb --if-exists -h ${POSTGRES_HOST} -U ${PG_USER} ${PG_DB_NAME}
createdb -h ${POSTGRES_HOST} -U ${PG_USER} ${PG_DB_NAME}

cat empty_schema-icms.sql | psql -h ${POSTGRES_HOST} -U ${PG_USER} -d ${PG_DB_NAME} >loadEmptySchema.log

# some more cleanup: 
echo "alter schema public owner to ${ICMS_COMMON_ROLE};" | psql -h ${POSTGRES_HOST} -U ${PG_USER} -d ${PG_DB_NAME} -a
echo "alter schema epr owner to ${ICMS_COMMON_ROLE};"    | psql -h ${POSTGRES_HOST} -U ${PG_USER} -d ${PG_DB_NAME} -a
# make sure we find the sequences for the tables in epr.*:
echo "ALTER ROLE ${ICMS_COMMON_ROLE} SET search_path = epr,public;" | psql -h ${POSTGRES_HOST} -U ${PG_USER} -d ${PG_DB_NAME} -a
echo "ALTER ROLE ${ICMS_EPR_ROLE} SET search_path = epr,public;"    | psql -h ${POSTGRES_HOST} -U ${PG_USER} -d ${PG_DB_NAME} -a
echo "ALTER ROLE ${ICMS_READER_ROLE} SET search_path = epr,public;" | psql -h ${POSTGRES_HOST} -U ${PG_USER} -d ${PG_DB_NAME} -a

echo "ALTER ROLE ${PG_USER} SET search_path = epr,public;" | psql -h ${POSTGRES_HOST} -U ${PG_USER} -d ${PG_DB_NAME} -a
