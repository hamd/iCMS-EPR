
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import xml.etree.ElementTree as ET
import time
import datetime

import sqlalchemy

from app import db
from app.models import EprUser, Shift

def makeDateTime(inString):

    dString, tString = inString.split()
    mon,day,yr = [int(x) for x in dString.split('/')]
    hr, min, sec = [int(x) for x in tString.split(':')]

    return datetime.datetime(yr,mon,day, hr,min, sec)

def updateShifts():

    start = time.time()
    tree = ET.parse('imports/shifts-2015.xml')
    root = tree.getroot()
    print( "reading and parsing file took %f sec" % (time.time()-start) )

    userList = db.session.query(EprUser.hrId).all()
    existingShifts = db.session.query(Shift.shiftId).all()

    start = time.time()
    nUpd = 0
    for shift in root.findall('SHIFT'):

        userId = int(shift.find('SHIFTER_ID').text)
        shiftId = int(shift.find('SHIFT_ID').text)
        shiftWeight = float(shift.find('WEIGHT').text)
        if shiftWeight > -1. :    # and shiftWeight < 1.:
            try:
                dbShift = db.session.query(Shift).filter_by(shiftId=shiftId).one()
                print( "updating fractional shift %2.2f found for %s " % (shiftWeight, dbShift.weight,) )
                dbShift.weight = shiftWeight
                db.session.commit()
                nUpd += 1
            except Exception as e:
                print( "ERROR updating shiftID %d - got %s " % (shiftId, str(e)) )

        if ( (userId,) not in userList ) :
            # print "skipping shift %s as user %s not found " % (shift.find('SHIFT_ID').text, shift.find('SHIFTER_ID').text)
            continue

        if ( (shiftId,) in existingShifts ) :
           # print "skipping shift %s - already in DB " % (shift.find('SHIFT_ID').text, )
           continue


    print( "updating %d shifts (out of %d) took %f sec" % ( nUpd, len(root.findall('SHIFT')), time.time()-start ) )

if __name__ == '__main__':

    from app import create_app
    app = create_app('default')
    with app.app_context():
        # Extensions like Flask-SQLAlchemy now know what the "current" app
        # is while within this block. Therefore, you can now run........
        # db.create_all()
        updateShifts()
