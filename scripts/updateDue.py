
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import time
import datetime

from app import db
from app.models import EprUser, EprDue

def updateDue():

    start = time.time()
    with open('imports/iCMS_Person.json', 'r') as instFile:
        users = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'len: ', len(users) )

    existingUsers = db.session.query(EprUser.cmsId).all()

    start = time.time()
    nOK = 0
    nNoUser = 0
    nSkipped = 0
    nAdded   = 0
    nUpdated = 0
    for u in users:

        if (int( u['cmsId'] ),) not in existingUsers:
            nNoUser += 1
            continue

        # skip people who do not have a due value set (i.e. they are already authors, or do not apply.
        if ( (float( u['epr_due_2015'] ) == 0.) and
             (float( u['epr_due_2016'] ) == 0.) ):
            nSkipped += 1
            continue

        user = db.session.query(EprUser).filter_by(cmsId = int(u['cmsId'])).one()

        for year in [2015, 2016]:
            # print "processing %s (%i) for %i" % (user.name, user.id, year)
            due = None
            try:
                due = db.session.query(EprDue).filter_by(userId = user.id, year=year).one()
                due.userId = user.id
                due.instId = user.mainInst
                due.workDue = float( u['epr_due_%s' % str(year)] )
                due.year = year
                due.timestamp = datetime.datetime.now()

                print( "updating entry for user %s, year %i" % (user.name.encode('ascii', 'xmlcharrefreplace'), year) )
                nUpdated += 1
            except Exception as e:
                if 'No row was found for one' in str(e):
                    pass # _continue_ with processing !
                else:
                    print( "ERROR: unknown error when selecting EPR due for %s (%s) - skipping " % (user.name.encode('ascii', 'xmlcharrefreplace'), str(e)) )
                    continue   # _skip_ further processing of this user

            if not due:
                due = EprDue(userId= user.id,
                             instId= user.mainInst,
                             workDue= float( u['epr_due_%s' % str(year)] ),
                             year=year
                             )
                db.session.add(due)
                nAdded += 1
                print( "added new entry for user %s, year %i" % (user.name.encode('ascii', 'xmlcharrefreplace'), year) )

            try:
                db.session.commit()
                nOK += 1
                print( '==> ', due )
            except Exception as e:
                print( "ERROR committing updated/new due entry for %s - got: %s" % (user.name.encode('ascii', 'xmlcharrefreplace'), str(e)) )
                db.session.rollback()

    print( "processing %i (%i) users (%i updated, %i added, %i skipped, %i notInDB) took %f sec." % (nOK, len(existingUsers), nUpdated, nAdded, nSkipped, nNoUser, time.time()-start) )

if __name__ == '__main__':
    updateDue()
