
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import time

def getTaskInfo():

    start = time.time()
    with open('imports/iCMS_Task.json', 'r') as projFile:
        tasks = json.load( projFile )
    print( "loading json took ", time.time() - start, 'sec' )

    taskInfo = {}
    for i in tasks:
        if int( i['year']) < 2014 : continue
        # if i['code'].strip() != '':
        #     print "found code for task:", str(i)
        if i['flag'] != "ACTIVE":
            print( "ignoring non-active task ", str(i) )
            continue

        taskInfo[ i['taskId'] ] = { 'shortName' : i['shortName'],
                                    'name' : i['name'],
                                    'activityId' : i['activityId'],
                                    'description' : i['description'],
                                    }

    return taskInfo

def checkPledges():
    
    start = time.time()
    with open('imports/iCMS_Pledge.json', 'r') as projFile:
        pledges = json.load( projFile )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i pledges' % len(pledges) )

    print( pledges[0] )

    taskInfo = getTaskInfo()

    start = time.time()
    nOK = 0
    for i in pledges:

        if i['year_str'].strip() == '' or int( i['year_str'] ) < 2015:
            # print "skipping old pledge", str(i)
            continue

#         print '\nprocessing pledge: ', str(i)

        nOK += 1
        tA = float( i['totalAccepted_str'] )
        tS = float( i['totalShift_str'] )
        tot = float( i['total_str'] )
        # if tA+tS != tot :
        #     print "found non-equal: totAcc %f + shift %f - tot %f" % (tA, tS, tot)

        amount = float( i['amount_str'] )
        # if amount != tot :
        #     print "found non-equal: amount %f - tot %f" % (amount, tot)

        if amount == 12.:
            print( str(i) )

    print( "checked %i (%i OK) pledges in %f sec." % (len(pledges), nOK, time.time()-start) )
    print( '\n' )

def doAll():
    checkPledges()

if __name__ == '__main__':
    doAll()
