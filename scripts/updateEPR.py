
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import time
import datetime

from app import db
from app.models import EprInstitute, EprUser, Role, Project, CMSActivity, Task, Pledge, Permission, Manager

from app.models import commitUpdated, commitNew

projMap = {}

def importProjects():
    
    start = time.time()
    with open('imports/iCMS_Project.json', 'r') as projFile:
        projects = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i projects' % len(projects) )

    print( projects[0] )

    start = time.time()
    nOK = 0
    nOld = 0
    for i in projects:

        if int( i['year'] ) < 2015:
            # print "skipping old project", str(i)
            nOld += 1
            continue

        print( '\nprocessing project: ', str(i) )

        iName = i['projectId']
        if not iName :
            iName = i['projectId']
        if not iName:
            print( "ERROR: Ignoring project w/o projectId : ", str(i) )
            continue

        try:
            proj = Project(name=i['name'].decode('latin-1').encode("utf-8"),
                           desc=i['name'].decode('latin-1').encode("utf-8"),
                           code=iName.decode('latin-1').encode("utf-8") )
            db.session.add(proj)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) projects in %f sec." % (len(projects), nOK, nOld, time.time()-start) )
    print( '\n' )


def getActMgt():

    start = time.time()
    with open('imports/iCMS_ActivityManagement.json', 'r') as projFile:
        actMgt = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    actProjMap = {}
    actMgrs = {}
    actIdMap = {}
    for i in actMgt:
        if int( i['year']) < 2014 : continue

        actIdMap[ i['id'] ] = '%s-%s' % ( i['projectId'], i['activityId'] )

        actProjMap[ i['activityId'] ] = i['projectId']
        actMgrs[ i['activityId'] ] = [ i['cmsId'] ]
        for index in ['cmsId2', 'cmsId3', 'cmsId4']:
            if i[index] != 0 :
                actMgrs[ i['activityId'] ].append(i[index])


    return actProjMap, actMgrs, actIdMap

def getActivityInfo():
    
    start = time.time()
    with open('imports/iCMS_Activity.json', 'r') as projFile:
        activities = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i activities' % len(activities) )

    actInfo = {}
    for i in activities:

# some activities from older times are still active. :(
#         if int( i['year'] ) < 2015: continue

        actId = i['activityId']
        if actId in actInfo.keys(): continue

        actInfo[actId] = { 'desc' : i['description'],
                           'name' : i['name'],
                           'shortName' : i['shortName'],
                           }

    return actInfo

def importActivities():

    start = time.time()
    with open('imports/iCMS_Activity.json', 'r') as projFile:
        activities = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i activities' % len(activities) )

    print( activities[0] )

    actProjMap, actMgrs, actIdMap = getActMgt()

    start = time.time()
    nOK = 0
    nOld = 0
    for i in activities:

        if int( i['year'] ) < 2015:
            # print "skipping old project", str(i)
            nOld += 1
            continue
        try:
            actIdString = '[%s-%s] %s' % ( actProjMap[ i['activityId'] ] ,
                                       i['activityId'],
                                       i['shortName'].decode('latin-1').encode("utf-8"))
        except KeyError:
            print( "ERROR: no project found in map for ", i['activityId'], 'skipping activity.' )
            continue

        actCode = '[%s-%s]' % ( actProjMap[ i['activityId'] ] , i['activityId'] )

        print( '\nprocessing activity named "' + actIdString + '" : ', str(i) )
        print( '        ...  code: ', actCode )

        iName = i['activityId']
        if not iName :
            iName = i['activityId']
        if not iName or iName.strip() == '':
            print( "ERROR: Ignoring activity w/o activityId : ", str(i) )
            continue

        proj = db.session.query(Project).filter_by(code = actProjMap[ i['activityId'].decode('latin-1').encode("utf-8") ] ).one()

        try:
            act = CMSActivity( name= i['shortName'].decode('latin-1').encode("utf-8"),
                               desc=i['name'].decode('latin-1').encode("utf-8"),
                               proj = proj,
                               code = actCode )
            db.session.add(act)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) activities in %f sec." % (len(activities), nOK, nOld, time.time()-start) )
    print( '\n' )


def getTaskInfo():

    start = time.time()
    with open('imports/iCMS_Task.json', 'r') as projFile:
        tasks = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    taskInfo = {}
    for i in tasks:
        if int( i['year']) < 2015 : continue
        # if i['code'].strip() != '':
        #     print "found code for task:", str(i)
        if i['flag'] != "ACTIVE":
            print( "ignoring non-active task ", str(i) )
            continue

        taskInfo[ i['taskId'] ] = { 'shortName' : i['shortName'],
                                    'name' : i['name'],
                                    'activityId' : i['activityId'],
                                    'description' : i['description'],
                                    'projId' : i['projectId'],
                                    'shiftType' : i['shiftType'],
                                    }

    return taskInfo

def updateShiftTasks():
    return importTasks(updateShiftTasks=True)

def importTasks(updateShiftTasks=False):
    
    start = time.time()
    with open('imports/iCMS_TaskManagement.json', 'r') as projFile:
        tasks = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i tasks' % len(tasks) )

    print( tasks[0] )

    actInfo = getActivityInfo()
    taskInfo = getTaskInfo()

    start = time.time()
    nActOK = 0
    nOK = 0
    nOld = 0
    for i in tasks:

        if int( i['year'] ) < 2015:
            # print "skipping old task", str(i)
            nOld += 1
            continue

        iName = i['taskId']
        if not iName or iName.strip() == '':
            print( "ERROR: Ignoring task w/o taskId : ", str(i) )
            continue

        if iName not in taskInfo.keys():
            print( "Ignoring task for %s as not found in active tasks " % iName )
            continue

        projId = i['projectId'].decode('latin-1').encode("utf-8")
        actId  = i['activityId'].decode('latin-1').encode("utf-8")

        tName = '[[%s-%s]-%s] %s' % ( projId, actId,
                                      i['taskId'],
                                      taskInfo[iName]['name'].decode('latin-1').encode("utf-8") )

        tCode = '[[%s-%s]-%s]' % ( projId, actId,
                                   i['taskId'] )

        try:
            actCode = '[%s-%s]' % (projId, actId)
            activity = db.session.query(CMSActivity).filter( CMSActivity.code.like( actCode ) ).one( )
        except Exception as e:
            if "No row was found for " in str(e):
                print( "No activity found for actCode '%s' (taskId %s) - creating activity" % (actCode+'%', iName) )
                # first get the project:
                proj = db.session.query(Project).filter_by(code = i['projectId'].decode('latin-1').encode("utf-8")).one()
                print( "got project ", proj )
                # now try to create the new activity:
                print( "going to create act for name %s desc %s code %s" % (actInfo[actId]['shortName'].decode('latin-1').encode("utf-8"),
                                                                           actInfo[actId]['name'].decode('latin-1').encode("utf-8"),
                                                                           actCode) )
                try:
                    act = CMSActivity( name = actInfo[actId]['shortName'].decode('latin-1').encode("utf-8"),
                                       desc = actInfo[actId]['name'].decode('latin-1').encode("utf-8"),
                                       proj = proj,
                                       code = actCode )
                    db.session.add(act)
                    db.session.commit()
                    nActOK += 1
                except Exception as e:
                    print( "ERROR: can not create CMSActivity code '%s' for task '%s-%s-%s' - got: '%s' " % (actCode, projId,actId,tCode, str(e)) )
                    db.session.rollback()
            else:
                print( "ERROR: DB error when retrieving CMSActivity %s for task %s-%s-%s - got: %s " % (actCode, projId,actId,tCode, str(e)) )


        # now re-retrieve it so it's available below
        activity = db.session.query(CMSActivity).filter( CMSActivity.code.like( actCode+'%' ) ).one( )

        if not updateShiftTasks:
            print( '\nprocessing task code %s name "%s":  %s', (tCode, tName, str(i)) )

            taskType = 'Perennial'
            sTypeId = taskInfo[iName]['shiftType']

            if sTypeId != '': taskType = 'central shift'
            if "on" in taskInfo[iName]['name'] and "call" in taskInfo[iName]['name']: taskType = 'on-call expert shift'

        if updateShiftTasks and sTypeId != '':
            print( "updating shift task, code: %s shiftTypeId %s " % (tCode, sTypeId) )
            task = db.session.query(Task).filter_by(code=tCode).one()
            task.tType = taskType
            task.shiftTypeId = sTypeId

            db.session.commit()
            nOK += 1
            continue

        try:
            task = Task(name = taskInfo[iName]['name'].decode('latin-1').encode("utf-8"),
                        desc = taskInfo[iName]['description'].decode('latin-1').encode("utf-8"),
                        act = activity,
                        needs = float( i['needed'] ),
                        pctAtCERN = 0,
                        tType = taskType,
                        comment = 'imported',
                        code = tCode,
                        level=1, parent='', 
                        shiftTypeId = sTypeId
                        )
            db.session.add(task)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) tasks in %f sec. Created %i new activities" % (len(tasks), nOK, nOld, time.time()-start, nActOK) )
    print( '\n' )

def importTasksOnly(updateShiftTasks=False):

    start = time.time()
    with open('imports/iCMS_TaskManagement.json', 'r') as projFile:
        tasks = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i tasks' % len(tasks) )

    print( tasks[0] )

    taskInfo = getTaskInfo()

    start = time.time()
    nOK = 0
    nOld = 0
    for i in tasks:

        if int( i['year'] ) < 2015:
            # print "skipping old task", str(i)
            nOld += 1
            continue

        iName = i['taskId']
        if not iName or iName.strip() == '':
            print( "ERROR: Ignoring task w/o taskId : ", str(i) )
            continue

        actCode = '[%s-%s]%%' % (i['projectId'].decode('latin-1').encode("utf-8"),
                                 i['activityId'].decode('latin-1').encode("utf-8"))
        try:
            activity = db.session.query(CMSActivity).filter( CMSActivity.code.like( actCode ) ).one( )
        except Exception as e:
            if "No row was found for " in str(e):
                print( "ERROR: no activity found for actCode %s (taskId %s) (skipping task)" % (actCode, iName) )
                continue

        if iName not in taskInfo.keys():
            print( "Ignoring task for %s as not found in active tasks " % iName )
            continue

        tName = '[[%s-%s]-%s] %s' % ( i['projectId'].decode('latin-1').encode("utf-8"),
                                      i['activityId'].decode('latin-1').encode("utf-8"),
                                      i['taskId'],
                                      taskInfo[iName]['name'].decode('latin-1').encode("utf-8") )

        tCode = '[[%s-%s]-%s]' % ( i['projectId'].decode('latin-1').encode("utf-8"),
                                   i['activityId'].decode('latin-1').encode("utf-8"),
                                   i['taskId'] )
        if not updateShiftTasks:
            print( '\nprocessing task code %s name "%s":  %s', (tCode, tName, str(i)) )

            taskType = 'Perennial'
            sTypeId = taskInfo[iName]['shiftType']

            if sTypeId != '': taskType = 'central shift'
            if "on" in taskInfo[iName]['name'] and "call" in taskInfo[iName]['name']: taskType = 'on-call expert shift'

        if updateShiftTasks and sTypeId != '':
            print( "updating shift task, code: %s shiftTypeId %s " % (tCode, sTypeId) )
            task = db.session.query(Task).filter_by(code=tCode).one()
            task.tType = taskType
            task.shiftTypeId = sTypeId

            db.session.commit()
            nOK += 1
            continue

        try:
            task = Task(name = taskInfo[iName]['name'].decode('latin-1').encode("utf-8"),
                        desc = taskInfo[iName]['description'].decode('latin-1').encode("utf-8"),
                        act = activity,
                        needs = float( i['needed'] ),
                        pctAtCERN = 0,
                        tType = taskType,
                        comment = 'imported',
                        code = tCode,
                        level=1, parent='',
                        shiftTypeId = sTypeId
                        )
            db.session.add(task)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()

    print( "imported %i (%i OK, %i old) tasks in %f sec." % (len(tasks), nOK, nOld, time.time()-start) )
    print( '\n' )

def readPledges():
    
    start = time.time()
    with open('imports/iCMS_Pledge.json', 'r') as projFile:
        pledges = json.load( projFile, encoding='latin-1' )
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i pledges' % len(pledges) )

    print( pledges[0] )

    plInDb = db.session.query(Pledge.code).all()
    print( plInDb[:10] )

    taskInfo = getTaskInfo()

    return pledges, plInDb, taskInfo

def findPledgeParams(i, verbose=True):

    task = None
    inst = None
    user = None

    try:
        taskCodeStr = '[[%s-%s]-%s]%%' % ( i['projectId'],
                                          i['activityId'],
                                          i['taskId'] )
        task = db.session.query(Task).filter( Task.code.like(taskCodeStr) ).one()
    except Exception as e:
        if "No row was found for " in str(e):
            if verbose: print( "ERROR: no task found for taskCode %s (pledgeId %s) (skipping pledge)" % (taskCodeStr, i['id']) )
            return task, inst, user
        else:
            if verbose: print( "ERROR: tryring to find taskId %s for pledge %s: %s" % (i['taskId'], i['id'], str(e)) )
            return task, inst, user
    try:
        inst = db.session.query(EprInstitute).filter_by(code=i['instCode']).one()
    except Exception as e:
        if "No row was found for " in str(e):
            if verbose: print( "ERROR: no institute (%s) found for taskId %s (pledgeId %s) (skipping pledge)" % (i['instCode'], i['taskId'], i['id']) )
            return task, inst, user
        else:
            if verbose: print( "ERROR: tryring to find institute (%s) for pledge %s: %s" % (i['instCode'], i['id'], str(e)) )
            return task, inst, user

    try:
        user = db.session.query(EprUser).filter_by( cmsId=int(i['cmsId']) ).one()
    except Exception as e:
        if "No row was found for " in str(e):
            if verbose: print( "ERROR: no user (%s) found for taskId %s (pledgeId %s) inst %s (skipping pledge)" % (i['cmsId'], i['taskId'], i['instCode'], i['id']) )
            return task, inst, user
        else:
            if verbose: print( "ERROR: tryring to find user (%s) for inst %s, pledge %s: %s" % (i['cmsId'], i['instCode'], i['id'], str(e)) )
            return task, inst, user

    return task, inst, user

def updatePledges():

    pledges, plInDb, taskInfo = readPledges()

    start = time.time()
    nOK = 0
    nOld = 0
    for i in pledges:

        if i['year'] < 2015:
            # print "skipping old pledge", str(i)
            nOld += 1
            continue

        print( '\nprocessing pledge: ', str(i) )

        task, inst, user = findPledgeParams(i)

        plCode = '+'.join([str(x) for x in [task.id, user.id, inst.id]])
        if (plCode,) not in plInDb:
            print( 'ERROR: pledge for code: %s should already exist, but not found in DB !??!?' % plCode )
            continue
        else:
            print( 'updating pledge for code %s ' % plCode )

        pledge = db.session.query(Pledge).filter_by(code = plCode).one()

        # update times by percentage. there are 145 (out of 5820) pledges which have this in 2015
        pledge.workTimePld = float(i['amount']) * ( float(i['percentage'])/100. )
        pledge.workTimeAcc = float(i['totalAccepted']) * ( float(i['percentage'])/100. )
        pledge.workTimeDone = float(i['total']) * ( float(i['percentage'])/100. )

        try:
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR committing updated pledge %s got: %s" % (plCode, str(e)) )
            db.session.rollback()

    print( "updated %i (%i OK, %i old) pledges in %f sec." % (len(pledges), nOK, nOld, time.time()-start) )
    print( '\n' )

def fixImportErrors():

# from the log, there were these categories of errors:
#
# - integrity error: just update the existing (multiple) pledge, latest entry is assumed to be highest "id" and wins
#
# - missing instiutes should be there now -- check "Add institute" page for name/year mismatch ...
#
# - missing users (exmembers ?) - ignore for now
#
# - users with negative cmsId: squash to one generic user for each instcode

    integErrs =  ['128+7714+257', '128+8032+193', '135+9993+224', '137+7039+235', '138+9024+216', '14+8126+2',
                  '142+5042+52', '149+3850+114', '160+4711+237', '187+9353+75', '2+3150+190', '25+490+202',
                  '26+7038+287', '26+8795+44', '29+7310+114', '30+4869+155', '30+9516+41', '335+2548+267',
                  '341+4395+31', '341+650+31', '344+8000+263', '345+8000+263', '365+8229+196', '366+5863+41',
                  '374+9301+147', '391+9333+227', '403+10053+152', '496+752+282', '500+9617+163', '502+9062+24',
                  '514+5645+255', '515+9068+109', '515+9494+263', '56+9328+148', '589+4964+52', '622+10039+242',
                  '689+5614+75', '703+8901+66', '704+9342+224', '704+9381+224', '704+9411+224', '707+3602+198',
                  '708+3602+198', '708+4040+49', '708+5867+138', '710+126+198', '711+3602+198', '713+9195+108',
                  '719+9937+224', '720+2579+2', '721+10048+14', '727+5511+75', '728+7646+1', '735+4790+216',
                  '746+2051+165', '753+4471+176', '753+9748+198', '754+5912+289', '760+5457+95', '760+8813+1',
                  '790+7299+13', '801+5183+227', '809+4192+150', '815+1865+39', '815+3195+217', '815+5292+95',
                  '815+8403+289', '843+8460+157', '87+4+284', '87+5622+284', '88+5138+284', '89+9941+111',
                  '9+7657+2', '90+6070+5'
                  ]

    pledges, plInDb, taskInfo = readPledges()

    start = time.time()
    nOK = 0
    nOld = 0
    for i in pledges:

        if int(i['year']) != 2015:
            # print "skipping old pledge", str(i)
            nOld += 1
            continue

        task, inst, user = findPledgeParams(i, verbose=False)

        if not task:
            print( "ERROR task %s is not defined for pledge %s " % (i['taskId'], i['id']) )
            continue

        if not inst:
            print( "ERROR inst %s not found for task %s pledge %s:" % (i['instCode'], i['taskId'], i['id']) )
            continue

        if not user:
            if int(i['cmsId']) > 0:
                print( "ERROR non-generic user with cmsId %s found, skipping pledge " % i['cmsId'] )
                continue

            print( "Going to create generic user for cmsId %s inst %s" % ( int(i['cmsId']), i['instCode']) )
            user = EprUser(cmsId=int(i['cmsId']),
                        username  = 'genU-%s-%s' % (inst.code, str(i['cmsId'])),
                        name      = '%s, Generic User %s' % (inst.code, str(i['cmsId'])),
                        hrId      = int(i['cmsId']),
                        instId    = int(inst.cernId),
                        authorReq = False,
                        status    = 'CMS',
                        isSusp    = True,
                        categoryName = "Engineer",
                        year = int(i['year']),
                        mainInst= int(inst.id)
                        )
            commitNew(user)

        plCode = '+'.join([str(x) for x in [task.id, user.id, inst.id]])
        if plCode not in integErrs : continue

        if (plCode,) not in plInDb:
            print( 'ERROR: pledge for code: %s should already exist, but not found in DB !??!?' % plCode )
            continue

        print( 'going to update pledge %s ' % plCode, i['year'], i['projectId'], i['activityId'], i['taskId'] )
        print( '     ... ', i )
        pl = db.session.query(Pledge).filter_by(code=plCode).one()
        print( '     ... ', pl )
        print( '' )

        pl.workTimePld = float(i['amount'])
        pl.workTimeAcc = float(i['totalAccepted'])
        pl.workTimeDone = float(i['total'])
        pl.status = 'done'
        pl.year = 2015
        pl.workedSoFar = float(i['total'])
        pl.timestamp = datetime.datetime.utcnow()
        commitUpdated(pl)

        nOK += 1

    print( "fixed %i out of %i (%i overall, %i old) pledges in %f sec." % (nOK, len(integErrs), len(pledges), nOld, time.time()-start) )
    print( '\n' )

    db.session.rollback()

def updateAll():
    updatePledges()
    # fixImportErrors()

if __name__ == '__main__':
    updateAll()

