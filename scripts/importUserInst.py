# -*- coding: utf-8 -*-

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import time
from sqlalchemy.exc import IntegrityError

from app import db
from app.models import EprInstitute, EprUser, Role, Category
from app.ldapAuth import getLogin

def importInst():

    start = time.time()
    with open('imports/iCMS_Institute.json', 'r') as instFile:
        institutes = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i institutes' % len(institutes) )

    print( institutes[0] )

    start = time.time()
    addedInst = []
    existingCernIds = []
    nOK = 0
    offset = 0
    fakeCernInstId = -10
    for i in institutes:
        print( '\nprocessing: ', str( i ).encode('ascii', 'xmlcharrefreplace') )

        # only add institutes which are actual members of CMS "right now" (at time of import)
        if i['dateCmsIn'] is None :
            print( "ignoring inst w/o entry date: ", str( i ).encode('ascii', 'xmlcharrefreplace') )
            continue

        if i['dateCmsOut'] is not None :
            print( "ignoring inst which left already: ", str( i ).encode('ascii', 'xmlcharrefreplace') )
            continue

        iName = None
        if i['shortName']:
            iName = i['shortName'].encode('ascii', 'xmlcharrefreplace')
        if not iName and i['name']:
            iName = i['name'].encode("utf-8")
        if not iName:
            print( "ERROR: Ignoring Institute w/o name or shortName : ", str(i).encode('ascii', 'xmlcharrefreplace') )
            continue

        if not i['cernInstId'] :
            fakeCernInstId -= 1
            print( "Handling Institute w/o cernInstID (new fake one is: %i) : %s " % (fakeCernInstId, str(i).encode('ascii', 'xmlcharrefreplace') ) )
            cernInstId = fakeCernInstId
        else:
            cernInstId = int( i['cernInstId'] )

        # handle duplicate cernInstIds:
        if (cernInstId,) in existingCernIds:
            offset += 1
            cernInstId = 1000*cernInstId+offset

        if cernInstId > 0:
            existingCernIds.append( cernInstId )

        countryName = i['country'].decode('latin-1').encode("utf-8")[0].upper()+i['country'].decode('latin-1').encode("utf-8")[1:].lower()

        db.session.autoflush = False

        # handle duplicate cernInstIds:
        comment = ''
        if cernInstId in addedInst:
            fakeCernInstId -= 1
            print( "handling duplicate cernInstId %i - set to %i " % (cernInstId, fakeCernInstId) )
            comment = "duplicate cernInstId: %i - set to %i " % (cernInstId, fakeCernInstId)
            cernInstId = fakeCernInstId

        inst = EprInstitute(cernId  = cernInstId,
                         name    = iName,
                         country = countryName,
                         code    = i['code'].decode('latin-1').encode("utf-8"),
                         comment = comment,
                         cmsStatus= i['cmsStatus']
                         )
        try:
            db.session.add(inst)
            db.session.flush()
        except Exception as e:
            print( "ERROR: flushing ... got: ", str(e) )
            db.session.rollback()
            continue

        try:
            db.session.commit()
            nOK += 1
            addedInst.append( cernInstId )
        except Exception as e:
            print( "ERROR: committing ... got: ", str(e) )
            db.session.rollback()
            try:
               inst = db.session.query(EprInstitute).filter_by(cernId=cernInstId).one()
            except Exception as e:
                db.session.rollback()
                print( "ERROR: no institute found for ", str(i) )
                continue

            if inst.cernId == cernInstId and \
               inst.name.lower() == iName.lower() and \
                inst.country.lower() == i['country'].lower() : # duplicate entry, ignore
                print( "duplicate found for %s, ignoring" % inst.name.encode('ascii', 'xmlcharrefreplace') )
            else:
                print( "ERROR inserting inst with : cernId %i shortname %s country %s " % ( cernInstId,
                                                                                           iName.encode('ascii', 'xmlcharrefreplace'),
                                                                                           i['country'].encode('ascii', 'xmlcharrefreplace') ) )
                print( "      found in DB         : %s " % str( db.session.query(EprInstitute).filter_by(cernId=cernInstId).all() ) )

    print( "imported %i (%i OK) institutes in %f sec." % (len(institutes), nOK, time.time()-start) )
    print( '\n' )

def createCategories():

    catInfo = { "Administrative" : False,
                "Doctoral Student" : True,
                "Engineer" : True,
                "Engineer Electronics" : True,
                "Engineer Mechanical" : True,
                "Engineer Software" : True,
                "Non-Doctoral Student" : False,
                "Other" : False,
                "Physicist" : True,
                "Technician" : False,
                "Theoretical Physicist" : True,
              }
    for cat, ok in catInfo.items():
        cat = Category(name=cat, ok=ok )
        try:
            db.session.add(cat)
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            if 'UNIQUE constraint failed' in str(e):
                pass
            else:
                print( "got error when creating categories" )
                raise e


def getCatNameMap():

    start = time.time()
    with open('imports/iCMS_MemberActivity.json', 'r') as instFile:
        cats = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    catNameMap = {}
    for i in cats:
        catNameMap[ i['id'] ] = i['name']

    return catNameMap


def convertName(firstName, lastName):

    fullName = None

    try:
        fullName = '%s, %s' % (lastName.encode('utf-8'),
                               firstName.encode('utf-8') )
        return fullName
    except Exception as e:
        print( "name conversion without decoding failed: %s" % str(e) )

    for coding in [ 'latin-1', 'iso-8859-1', 'iso-8859-15']:
        try:
            fullName = '%s, %s' % (lastName.decode(coding).encode('utf-8', 'xmlcharrefreplace'),
                                   firstName.decode(coding).encode('utf-8', 'xmlcharrefreplace'))
            return fullName
        except Exception as e:
            print( "name conversion with %s failed: %s" % (coding, str(e)) )
            continue
    return None

def updateUserAuthReq():

    start = time.time()
    with open('imports/iCMS_Person.json', 'r') as instFile:
        users = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'len: ', len(users) )

    print( users[0] )

    existingUsers = db.session.query(EprUser.cmsId).all()
    catNameMap = getCatNameMap()

    start = time.time()
    nExMem = 0
    nOK = 0
    for u in users:

        cmsId = int(u['cmsId'])
        if (cmsId,) not in existingUsers: continue

        uDB = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
        uDB.authorReq = int( u[ 'isAuthor' ] )

    try:
        db.session.commit()
    except Exception as e:
        print( "ERROR updating authorReq for user %i, got: %s" % (cmsId, str(e)) )
        db.session.rollback()

    print( "done" )
    return

def importUsers():

    # # add a default admin for testing:
    # a = User(name='ap', hrId=0, cmsId=0, instId=64931)
    # aRole = db.session.query(Role).filter_by(name='Administrator').first()
    # a.role_id = aRole.id
    # db.session.add(a)

    start = time.time()
    with open('imports/iCMS_Person.json', 'r') as instFile:
        users = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'len: ', len(users) )

    print( users[0] )

    existingUsers = db.session.query(EprUser.cmsId).all()

    catNameMap = getCatNameMap()

    start = time.time()
    nExMem = 0
    nOK = 0
    fakeUserNameIndex = 0
    for u in users:

        if (int( u['cmsId'] ),) in existingUsers:
            # print "skipping already existing user ", str(u).encode('ascii', 'xmlcharrefreplace')
            continue

        iCode = u['instCode'].encode('ascii', 'xmlcharrefreplace')
        # if 'exmember' in u['status'].lower() :
        #     nExMem += 1
        #     continue # ignore exmembers for now

        print( '\n updating user ', u['cmsId'] )
        try:
            inst = db.session.query(EprInstitute).filter_by(code=iCode).one()
        except Exception as e:
            if 'No row was found for one()' in str(e) and \
                'exmember' in u['status'].encode('ascii', 'xmlcharrefreplace').lower() :
                print( 'IGNORING exmember %s,%s (%s) as no main institute could be found' % (
                        u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                        u['instCode'].encode('ascii', 'xmlcharrefreplace').lower() ) )
                continue
            else:
                print( "ERROR from DB when looking for institute id %s, user %s (skipping) : %s" % (str(iCode), str(u).encode('ascii', 'xmlcharrefreplace'), str(e)) )
            continue

        fullName = convertName(u['firstName'], u['lastName'])
        if not fullName:
            print( "name could not be converted, skipping ", u['cmsId'] )
            continue

        if not u['activityId']:
            print( "user w/o category: %s -- ignoring ", u['cmsId'] )
            continue

        if not u['niceLogin']:
            userName = 'fakeUsername-%i' % (fakeUserNameIndex)
            fakeUserNameIndex += 1
        else:
            userName = u['niceLogin']

        hrIdFound = int(u['hrId'])
        if hrIdFound < 0: hrIdFound -= 1

        print( "creating user %s " %  u['cmsId'] )
        user = EprUser( username = userName,
                     name = fullName,
                     hrId = hrIdFound,
                     cmsId = int(u['cmsId']),
                     instId = int(inst.cernId),
                     authorReq= int( u[ 'isAuthor' ] ),
                     status= u['status'],
                     isSusp= u['isAuthorSuspended'],
                     categoryName= catNameMap[ u['activityId'] ],
                        mainInst=int(inst.id)
                     )
        try:
            db.session.add(user)
            db.session.commit()
            nOK += 1
        except Exception as e:
            print( "ERROR: got: ", str(e) )
            db.session.rollback()
            print( "ERROR inserting user with : name %s,%s hrId %i cernId %i inst %s " % (
                         u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                         u['hrId'],u['cmsId'],
                         u['instCode'].encode('ascii', 'xmlcharrefreplace')) )
            print( "      check if already in returned: %s " % str( db.session.query(EprUser).filter_by(cmsId=u['cmsId']).all() ).encode('ascii', 'xmlcharrefreplace') )

    print( "imported %i (%i OK) users in %f sec." % (len(users), nOK, time.time()-start) )
    print( "found %i exmembers" % nExMem )

def showInst(limit=10):

    import pprint
    inst = db.session.query(EprInstitute).all()
    if limit < 0:
        pprint.pprint( inst )
    else:
        pprint.pprint( inst[:limit] )

def importAll():
    createCategories()
    importInst()
    importUsers()

if __name__ == '__main__':
    importAll()
