#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from manage import theApp as app, db
from app.models import EprUser, EprInstitute, TimeLineUser, commitUpdated


def dummy():
    print( db.session.query(TimeLineUser).count() )

def yummy():
    wrongs = db.session.query(TimeLineUser, EprUser, EprInstitute).filter(TimeLineUser.year==2016).\
        filter(TimeLineUser.instCode.like('TIFR%')).join(EprUser, TimeLineUser.cmsId == EprUser.cmsId).\
        join(EprInstitute, EprUser.mainInst == EprInstitute.id).filter(TimeLineUser.instCode != EprInstitute.code).\
        filter(EprInstitute.code.in_(['TIFR-A', 'TIFR-B'])).all()
    for time_line, user, inst in wrongs:
        print( 'Got a time line [# %d] associated with %s and a user from %s and year %d' % (time_line.id, time_line.instCode, inst.code, time_line.year) )
        time_line.instCode = inst.code
        commitUpdated(time_line)
    print( 'Fixed %d time lines!' % len(wrongs) )


if __name__ == '__main__':
    with app.app_context():
        yummy()
