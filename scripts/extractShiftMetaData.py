
#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import sys
import os
import json

scrPath = '.'
if scrPath not in sys.path:
    sys.path.append(scrPath)


# From Frank, Oct 18, 2017:
# New tables to manage the shift weights (from Frank Glege, 18-Oct-2017):
# cms_shiftlist.shifttypes  : contains details on which shifts belongs to which "System"
# cms_shiftlist.shiftgrid   : contains the templates which are defined for each shift-entity
# to be used when implementing "conversion scaling" ... likely together with parsing/interpreting the date/time
# to see if a shift is of "day/evening/night" and "week/weekend" type.
# Q: where are the flavour_names ???

# for now, as this info is rather static, we simply "import" this from a json export from sqldeveloper
# and reformat it a bit.

inPath = 'export.json'
outPath = 'shiftMetaInfo.json'

all = json.load( open(inPath, 'r') )

outInfo = {}
for item in all['items']:

    subSys = item['sub_system']
    shiftType = item['shift_type']
    shiftType_id = item['shift_type_id']
    orderNum = item[ 'order_num' ]
    print( "processing item: %s -- '%s', '%s', '%s' " % (item, subSys, shiftType, orderNum) )
    if subSys in outInfo:
        if shiftType in outInfo[ subSys ]:
            if orderNum > outInfo[ subSys ][shiftType]:
                outInfo[ subSys ][shiftType] = orderNum
        else:
            outInfo[ subSys ].update( { shiftType : orderNum } )
    else:
        outInfo[ subSys ] = { shiftType : orderNum  }

import pprint
pprint.pprint( outInfo )

json.dump( outInfo, open(outPath, 'w'), sort_keys=True, indent=4 )

