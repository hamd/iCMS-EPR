#!/usr/bin/env bash

dropdb --if-exists TestDB
createdb TestDB
echo "TestDB recreated ... "

source ~/work/venvs/iCMS-EPR/bin/activate

export ICMS_EPR_TESTING=`hostname`
export ICMS_EPR_SECRET_KEY=b5a148f21cf29e6a85a0e5b4ccf925ac
export TEST_DATABASE_URL=postgres://ap@localhost/TestDB

#python manage.py shell << EOF
#import SetupDummies
#SetupDummies.resetDB( )
#SetupDummies.setup( )
#print "DB set up ..."
#EOF

