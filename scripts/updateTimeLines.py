# -*- coding: utf-8 -*-

#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import json
import time, datetime
from sqlalchemy.exc import IntegrityError

from app import db
from app.models import Category, EprInstitute, EprUser, TimeLineInst, TimeLineUser
from app.ldapAuth import getLogin

addedInst = []
exitingCernIds = []
fakeCernInstId = -10

def updateTimeLineInst(selInstCode=None, year=2016, addNew=False, dryRun=False):

    start = time.time()
    with open('imports/iCMS_Institute.json', 'r') as instFile:
        institutes = json.load(instFile, encoding='latin-1')
    print("loading json took ", time.time() - start, 'sec')

    print('found %i institutes in json file.' % len(institutes))

    global exitingCernIds

    # get all, no selection on year
    exitingInsts = db.session.query(EprInstitute.code).all()
    exitingCernIds = db.session.query(EprInstitute.cernId).all()

    print( "%i institutes, %i users in DB" % ( len(exitingInsts), len(exitingCernIds) ) )

    start = time.time()

    addedInst = []
    addedInstTL = []
    nOK = 0
    offset = 0

    if selInstCode:
        print( "Updating time line only for selected institute, code='%s'" % selInstCode )
        if (selInstCode,) in exitingInsts:
            print( "selected institute ", selInstCode, 'already in DB. Exiting.' )
            return

    for i in institutes:

        iCode = i['code'].decode('latin-1').encode("utf-8")

        if iCode == 'ZZZ': continue

        if selInstCode and selInstCode.lower() not in iCode.lower():
            continue

        print( '\nprocessing: ', str( i ).encode('ascii', 'xmlcharrefreplace') )

        if u'Yes' not in i['cmsStatus']:
            print( "***> skipping non-active institute code %s - status %s " % (iCode, i['cmsStatus']) )
            continue

        #-?? check if dateCmsIn/dateCmsOut are set and valid for year ??

        iName = None
        if i['shortName']:
            iName = i['shortName'].encode('utf-8')    # .encode('ascii', 'xmlcharrefreplace')
        if not iName and i['name']:
            iName = i['name'].encode("utf-8")
        if not iName:
            print( "ERROR: Ignoring Institute w/o name : ", str(i).encode('ascii', 'xmlcharrefreplace') )
            continue

        comment = ''

        tlInst = None
        try:
            tlInst = db.session.query(TimeLineInst).filter_by(code=iCode,year=year).one()
        except Exception as e:
            if "No row was found for one" in str(e):
                pass
            else:
                raise e
        if tlInst:

            tlInst.cmsStatus= i['cmsStatus']
            tlInst.comment = comment

            print( " ...  updated tlInst: ", tlInst )
            try:
                db.session.commit()
                nOK += 1
            except Exception as e:
                print( "ERROR: got: ", str(e) )
                db.session.rollback()
                print( "ERROR updating tlInst code %s with : cmsStatus %s, comment %s " % (iCode, i['cmsStatus'], comment) ) 

        else: # no inst found in TL, add a new one
            if (iCode,) not in exitingInsts:
                if addNew:
                    print( "adding new Institue code %s" % iCode )
                    newInstId = addNewInst( i, iCode, iName, year, comment, dryRun )
                    addedInst.append( newInstId )

                else:
                    print( "found unknown institute code %s - skipping, as addNew is not set " % iCode )
                    continue

            db.session.autoflush = False

            inst = TimeLineInst(code    = i['code'].decode('latin-1').encode("utf-8"),
                                year=year,
                                cmsStatus= i['cmsStatus'],
                                comment = comment,
                                )
            if not dryRun:
                try:
                    db.session.add(inst)
                    db.session.flush()
                    addedInstTL.append( iCode )
                except Exception as e:
                    print( "ERROR: flushing newly added inst %s ... got: %s " % (inst, str(e)) )
                    db.session.rollback()
                    continue

                try:
                    db.session.commit()
                    nOK += 1
                except Exception as e:
                    print( "ERROR: committing ... got: ", str(e) )
                    db.session.rollback()
                    try:
                       inst = db.session.query(TimeLineInst).filter_by(code=iCode, year=year).one()
                    except Exception as e:
                        db.session.rollback()
                        print( "ERROR: no institute found for ", str(i) )
                        continue

                    if inst.name.lower() == iName.lower() and \
                       inst.country.lower() == i['country'].lower() : # duplicate entry, ignore
                        print( "duplicate found for %s, ignoring" % inst.name.encode('ascii', 'xmlcharrefreplace') )
                    else:
                        print( "ERROR inserting inst with : code %s " % ( iCode,) )
                        print( "      found in DB         : %s " % str( db.sesion.query(TimeLineInst).filter_by(code=iCode, year=year).all() ) )
            else:
                db.session.rollback()
                nOK += 1
                print( "** dryRun ** ", )

    print( "imported %i (%i OK) institutes in %f sec." % (len(institutes), nOK, time.time()-start) )
    print( '\n' )

def addNewInst( i, iCode, cernInstId, iName, year, comment, dryRun=False):

    global fakeCernInstId
    global exitingCernIds

    print( "addNewInst> going to add new institute code %s status %s " % (iCode, i['cmsStatus'] ) )

    countryName = i['country'].decode('latin-1').encode("utf-8")[0].upper()+i['country'].decode('latin-1').encode("utf-8")[1:].lower()

    if not i['cernInstId'] :
        fakeCernInstId -= 1
        print( "Handling Institute w/o cernInstID (new fake one is: %i) : %s " % (fakeCernInstId, str(i).encode('ascii', 'xmlcharrefreplace') ) )
        cernInstId = fakeCernInstId
    else:
        cernInstId = int( i['cernInstId'] )

    # handle duplicate cernInstIds:
    comment = ''
    if (cernInstId,) in exitingCernIds:
        fakeCernInstId -= 1
        print( "handling duplicate cernInstId %i - set to %i " % (cernInstId, fakeCernInstId) )
        comment = "duplicate cernInstId: %i - set to %i " % (cernInstId, fakeCernInstId)
        cernInstId = fakeCernInstId

    if cernInstId in addedInst:
        print( "ERROR duplicate found for %i after handling dups ! - this should not happen!! skipping ! " % cernInstId )
        return  False

    if cernInstId in addedInst:
        print( "ERROR duplicate found for %i after handling dups ! - this should not happen!! skipping ! " % cernInstId )
        return False

    inst = EprInstitute(cernId  = cernInstId,
                         name    = iName,
                         country = countryName,
                         code    = i['code'].decode('latin-1').encode("utf-8"),
                         comment = comment,
                         cmsStatus= i['cmsStatus'],
                         year=year
                         )
    if not dryRun:
        try:
            db.session.add(inst)
            db.session.flush()
        except Exception as e:
            print( "ERROR: flushing newly added inst %s ... got: %s " % (inst, str(e)) )
            db.session.rollback()
            return False

        try:
            db.session.commit()
            addedInst.append( cernInstId )
        except Exception as e:
            print( "ERROR: committing ... got: ", str(e) )
            db.session.rollback()
            try:
               inst = db.session.query(EprInstitute).filter_by(cernId  = cernInstId).one()
            except Exception as e:
                db.session.rollback()
                print( "ERROR: no institute found for ", str(i) )
                return False

            if inst.cernId == cernInstId and \
               inst.name.lower() == iName.lower() and \
                inst.country.lower() == i['country'].lower() : # duplicate entry, ignore
                print( "duplicate found for %s, ignoring" % inst.name.encode('ascii', 'xmlcharrefreplace') )
            else:
                print( "ERROR inserting inst with : cernId %i shortname %s country %s " % ( cernInstId,
                                                                                           iName.encode('ascii', 'xmlcharrefreplace'),
                                                                                           i['country'].encode('ascii', 'xmlcharrefreplace') ) )
                print( "      found in DB         : %s " % str( db.session.query(EprInstitute).filter_by(cernId  = cernInstId).all() ) )
    else:
        db.session.rollback()
        print( "** dryRun ** ", )

    return True

def createCategories():

    catInfo = { "Administrative" : False,
                "Doctoral Student" : True,
                "Engineer" : True,
                "Engineer Electronics" : True,
                "Engineer Mechanical" : True,
                "Engineer Software" : True,
                "Non-Doctoral Student" : False,
                "Other" : False,
                "Physicist" : True,
                "Technician" : False,
                "Theoretical Physicist" : True,
              }
    for cat, ok in catInfo.items():
        cat = Category(name=cat, ok=ok )
        try:
            db.session.add(cat)
            db.session.commit()
        except IntegrityError as e:
            db.session.rollback()
            if 'UNIQUE constraint failed' in str(e):
                pass
            else:
                print( "got error when creating categories" )
                raise e

def getCatNameMap():

    start = time.time()
    with open('imports/iCMS_MemberActivity.json', 'r') as instFile:
        cats = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    catNameMap = {}
    for i in cats:
        catNameMap[ i['id'] ] = i['name']

    return catNameMap

def convertName(firstName, lastName):

    fullName = None

    try:
        fullName = '%s, %s' % (lastName.encode('utf-8'),
                               firstName.encode('utf-8') )
        return fullName
    except Exception as e:
        print( "name conversion without decoding failed: %s" % str(e) )

    for coding in [ 'latin-1', 'iso-8859-1', 'iso-8859-15']:
        try:
            fullName = '%s, %s' % (lastName.decode(coding).encode('utf-8', 'xmlcharrefreplace'),
                                   firstName.decode(coding).encode('utf-8', 'xmlcharrefreplace'))
            return fullName
        except Exception as e:
            print( "name conversion with %s failed: %s" % (coding, str(e)) )
            continue
    return None

def updateUserAuthReq():

    start = time.time()
    with open('imports/iCMS_Person.json', 'r') as instFile:
        users = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'len: ', len(users) )

    print( users[0] )

    existingUsers = db.session.query(EprUser.cmsId).all()
    catNameMap = getCatNameMap()

    start = time.time()
    nExMem = 0
    nOK = 0
    for u in users:

        cmsId = int(u['cmsId'])
        if (cmsId,) not in existingUsers: continue

        uDB = db.session.query(EprUser).filter_by(cmsId=cmsId).one()
        uDB.authorReq = int( u[ 'isAuthor' ] )

    try:
        db.session.commit()
    except Exception as e:
        print( "ERROR updating authorReq for user %i, got: %s" % (cmsId, str(e)) )
        db.session.rollback()

    print( "done" )
    return

def updateTimeLineUser(selCmsId=None, selInstCode=None, year=2016, addNew=False, dryRun=True):

    start = time.time()
    with open('imports/iCMS_Person.json', 'r') as instFile:
        users = json.load(instFile, encoding='latin-1')
    print( "loading json took ", time.time() - start, 'sec' )

    print( 'found %i users in json file ' % len(users) )

    # get all users for all years, so we can skip the ones not known at all ...
    existingUsers = db.session.query(EprUser.cmsId).all()

    print( "found %i users in DB " % (len(existingUsers),) )

    catNameMap = getCatNameMap()

    if selInstCode:
        print( "Updating only users for institute ", selInstCode )

    if selCmsId:
        print( "Updating only user for cmsId %i " % selCmsId )

    start = time.time()
    nExMem = 0
    nOK = 0
    nUnknown = 0

    authDues = { 2015 : 4.5, 2016 : 3 }

    for u in users:

        if selCmsId and int(selCmsId) != int(u['cmsId']): continue

        iCode = u['instCode'].encode('utf-8')  # .encode('ascii', 'xmlcharrefreplace')

        if selInstCode and selInstCode.lower() != iCode.lower():
            continue

        if selInstCode:
            print( '\n found instcode ', iCode )

        if not addNew and (int( u['cmsId'] ),) not in existingUsers:
            nUnknown += 1
            print( "addNew not set, so skipping update of non-existing user cmsId %s " % ( u['cmsId'],) ) # , str(u).encode('ascii', 'xmlcharrefreplace')
            continue

        print( '\n processing user %s iCode %s ' % (u['cmsId'], iCode) )

        try:
            inst = db.session.query(TimeLineInst).filter_by(code=iCode, year=year).one()
        except Exception as e:
            if 'No row was found for one()' in str(e) and \
                'exmember' in u['status'].encode('ascii', 'xmlcharrefreplace').lower() :
                print( 'IGNORING exmember %s,%s (%s) as no main institute could be found' % (
                        u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                        u['instCode'].encode('ascii', 'xmlcharrefreplace').lower() ) )
                continue
            else:
                print( "ERROR from DB when looking for institute code %s, user %s (skipping) : %s" % (str(iCode), str(u).encode('ascii', 'xmlcharrefreplace'), str(e)) )
            continue

        print( "found institute code %s id %i for year %i" % (inst.code, inst.id, inst.year) )

        fullName = convertName(u['firstName'], u['lastName'])
        if not fullName:
            print( "name could not be converted, skipping " )
            continue

        if not u['activityId']:
            print( "user w/o category: %s -- ignoring ", u['cmsId'] )
            continue

        print( "processing user %s (%s) for %i" %  (u['cmsId'], fullName, year) )

        user = None
        try:
            user = db.session.query(TimeLineUser).filter_by(cmsId=int(u['cmsId']),instCode=inst.code, year=year).one()
        except Exception as e:
            if "No row was found for one" in str(e):
                pass
            else:
                raise e

        category = db.session.query(Category).filter_by( name=catNameMap[u['activityId']] ).one()
        if user:
            print( "found user: ", user, ' going to update ...' )

            user.isAuthor    = bool( u[ 'isAuthor' ] )
            user.status      = u['status']
            user.isSuspended = bool(u['isAuthorSuspended'])
            user.category    = category.id
            user.year        = year

            user.yearFraction = 1. #-toDo: update this for the future

            user.dueAuthor = 0
            if user.isAuthor:
                user.dueAuthor = authDues[year]

            user.dueApplicant = 0

            user.timestamp = datetime.datetime.utcnow()

            print( " ...  updated user: ", user )

            try:
                db.session.commit()
                nOK += 1
            except Exception as e:
                print( "ERROR: got: ", str(e) )
                db.session.rollback()
                print( "ERROR updating user with : name %s,%s hrId %i cernId %i inst %s " % (
                             u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                             u['hrId'],u['cmsId'],
                             u['instCode'].encode('ascii', 'xmlcharrefreplace')) )

        else: # user not found for this year, create ...

            if (int( u['cmsId'] ),) not in existingUsers:
                createNewUser(u, fullName, year, inst.code)

            print( 'user %s not found, going to create ...' % (fullName, ) )

            dueAuthor = 0.
            if bool( u[ 'isAuthor' ] ):
                dueAuthor = authDues[year]

            dueApplicant = 0.
            if year == 2015: dueApplicant = float( u['epr_due_2015'] )
            if year == 2016: dueApplicant = float( u['epr_due_2016'] )

            user = TimeLineUser(cmsId = int(u['cmsId']),
                                instCode = inst.code,
                                year=year,
                                isAuthor = bool( u[ 'isAuthor' ] ),
                                isSuspended= bool(u['isAuthorSuspended']),
                                status= u['status'],
                                category= category.id,
                                dueAuthor = dueAuthor,
                                dueApplicant = dueApplicant,
                                yearFraction = 1.,    #-toDo: update this for the future
                                )

            try:
                db.session.add(user)
                db.session.commit()
                nOK += 1
            except Exception as e:
                print( "ERROR: got: ", str(e) )
                db.session.rollback()
                print( "ERROR updating user with : name %s,%s hrId %i cernId %i inst %s " % (
                             u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                             u['hrId'],u['cmsId'],
                             u['instCode'].encode('ascii', 'xmlcharrefreplace')) )

    print( "imported %i (of %i, skipped %i unknown users) users in %f sec." % (nOK, len(users), nUnknown, time.time()-start) )
    print( "found %i exmembers" % nExMem )


def createNewUser(u, fullName, year, instCode):

    cernInst = db.session.query(EprInstitute).filter_by(code=instCode).one()

    print( 'user %s not found, going to create ...' % (fullName, ) )

    catNameMap = getCatNameMap()

    user = EprUser(
                username = u['niceLogin'],
                name = fullName,
                hrId = int(u['hrId']),
                cmsId = int(u['cmsId']),
                instId = cernInst.cernId,
                authorReq= bool( u[ 'isAuthor' ] ),
                status= u['status'],
                isSusp= bool(u['isAuthorSuspended']),
                categoryName= catNameMap[ u['activityId'] ],
                year=year,
                mainInst=cernInst.id
                )

    try:
        db.session.add(user)
        db.session.commit()
    except Exception as e:
        print( "ERROR: got: ", str(e) )
        db.session.rollback()
        print( "ERROR updating user with : name %s,%s hrId %i cernId %i inst %s " % (
                     u['lastName'].encode('ascii', 'xmlcharrefreplace'),u['firstName'].encode('ascii', 'xmlcharrefreplace'),
                     u['hrId'],u['cmsId'],
                     u['instCode'].encode('ascii', 'xmlcharrefreplace')) )
        return False

    return True

def showInst(year=2016, limit=10):

    import pprint
    inst = db.session.query(TimeLineInst).filter_by(year=year).all()
    print( "found %i institutes for %i " % (len(inst), year) )
    if limit < 0:
        pprint.pprint( inst )
    else:
        pprint.pprint( inst[:limit] )

def showUsers(year=2016, limit=10):

    import pprint
    user = db.session.query(TimeLineUser).filter_by(year=year).all()
    print( "found %i users for %i" % (len(user), year) )
    if limit < 0:
        pprint.pprint( user )
    else:
        pprint.pprint( user[:limit] )

    auth = db.session.query(TimeLineUser).filter_by(isAuthor=True, year=year).all()
    print( "found %i authors for %i" % (len(user), year) )

def doAll(year=2016):
    for year in [2015,2016]:
        updateTimeLineInst(year=year, addNew=True)
        updateTimeLineUser(year=year, addNew=True)

if __name__ == '__main__':
   doAll()
