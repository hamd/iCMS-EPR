#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import os
import time
from smtplib import SMTP

import json

from threading import Thread
from flask import current_app, render_template

from icms_orm.common import EmailMessage, EmailLog

from . import db

def send_email(to, subject, template, **kwargs):

    if not to:
        msg = 'send_email> no addressees given (subj:%s), ignoring ... ' % subject
        current_app.logger.warning(msg)
        return msg

    subjectWithPrefix = subject
    subjPref = current_app.config['ICMS_MAIL_SUBJECT_PREFIX']
    if not subjectWithPrefix.startswith(subjPref):
        subjectWithPrefix = '%s %s' % (subjPref, subject)

    return send_email_detail( to,
                              subject = subjectWithPrefix, 
                              sender = current_app.config['ICMS_MAIL_SENDER'], 
                              body = render_template(template + '.txt', **kwargs), 
                              bcc = current_app.config['ICMS_MAIL_BCC'], 
                              reply_to = current_app.config['ICMS_MAIL_REPLY_TO'], 
                              source_app = 'epr')


def send_email_detail( to, subject, sender, body, bcc, reply_to, source_app = 'epr'):

    toArg = to
    if type(to) == type([]) or type(to) == type(set()):
        toArg =  ','.join(to)

    email, log = EmailMessage.compose_message( sender=sender, 
                                               bcc=bcc, 
                                               reply_to=reply_to, 
                                               subject=subject, 
                                               source_app=source_app, 
                                               to = toArg, 
                                               body = body,
                                               db_session=None, # do not yet send, we'll need to check if it's already done ...
                                             )

    res = db.session.query(EmailMessage).filter_by(hash=email.hash).all()
    msg =  "found %s existing mail(s) with hash %s " % (len(res), email.hash)
    if res:
        oldLog = db.session.query(EmailLog).filter_by(email_id=res[0].id).first()
        current_app.logger.warning( msg )
        current_app.logger.debug( 'A mail was already sent for this subject (%s) on %s' % (subject, oldLog.timestamp) )
        return False, msg

    try:
        db.session.add( email )
        db.session.add( log )
        db.session.commit()
    except Exception as e:
        msg = "Exception caught when trying to add email to DB: %s" % str(e)
        current_app.logger.error(msg)
        return False, msg

    msg = "Mail for subject %s sent to %s " % (subject, to)
    current_app.logger.info( msg )
    return True, msg
