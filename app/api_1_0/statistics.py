import time
import datetime
import json

from flask import jsonify, request, current_app, url_for
from flask_login import login_required, current_user

from . import api
from .. import db, cache

from sqlalchemy import and_, orm, case

from ..models import EprInstitute, AllInstView, TimeLineUser, TimeLineInst, EprUser, Project, CMSActivity, commitUpdated
from ..main.Helpers import getInstituteInfo, getInstAuthForProj

from ..main.Helpers import getPledgeYear, getInstUsers

from sqlalchemy_extensions import group_concat, QueryFactory

@api.route('/instSummary', methods=['GET', 'POST'])
@login_required
def instSummary():

    yearList = range(2016, datetime.date.today().year)
    res = (db.session.query(AllInstView.year, AllInstView.code, EprInstitute.country, 
                            AllInstView.actAuthors, AllInstView.doneFract, EprInstitute.cmsStatus)
                     .join(EprInstitute, EprInstitute.code == AllInstView.code)
                     .filter( AllInstView.year.in_( yearList ) )
                     .group_by( AllInstView.code, EprInstitute.country, EprInstitute.cmsStatus, AllInstView.year, AllInstView.doneFract, AllInstView.actAuthors )
                     .order_by( AllInstView.code )
                     .all())


    data = {}
    for year, code, country, nAuth, eprDoneFrac, status in res:
        # if year not in years: years.append(year)
        # if code not in codes: codes.append(code)
        if code not in data:
            data[ code ] = { 'country': country, 
                             'status': status if status != 'Yes' else 'Full', 
                             'nAuth': { year: nAuth }, 'eprDoneFrac' : { year: eprDoneFrac } }
        else:
            data[ code ]['nAuth'].update( { year: nAuth } )
            data[ code ]['eprDoneFrac'].update( { year: eprDoneFrac } )

    summ = []
    for code, v in data.items():
        row = [ '', code, v['country'], v['status'] ]
        for yr in yearList:
            row += [ '%.2f' % v['nAuth'][yr] if yr in v['nAuth'] else 0., 
                     '%.2f' % v['eprDoneFrac'][yr] if yr in v['eprDoneFrac'] else 0. ]
        summ.append( row )

    return jsonify( { 'data' : summ, 'yearList': list(yearList), 'raw': data }  )
