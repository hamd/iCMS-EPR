from flask import jsonify, request, current_app, flash, abort
from flask_login import login_required, current_user

from sqlalchemy import and_

from . import api
from .. import db
from ..models import Permission, Task, Manager, EprUser, CMSActivity, Level3Name, getTaskShiftTypeIds, commitUpdated

from ..main.Helpers import getTaskActivitiesForProject, getPledgeYear

from ..main.MailHelpers import getMgrEmails

@api.route('/activities')
@login_required
def getAllActivities():

    data = []
    for a in db.session.query(CMSActivity).all():
        data.append( a.to_json() )

    return jsonify( { 'data' : data } )

@api.route('/activity/<int:id>')
@login_required
def get_activity(id):
    activity = db.session.query(CMSActivity).filter(CMSActivity.id==id).one()
    if not activity:
        return abort(404)
    return jsonify(activity.to_json())

@api.route('/activityByName/<string:name>', methods=['GET', 'POST'])
@login_required
def get_activityByName(name):

    year = request.form.get('year', getPledgeYear())

    try:
        activity = db.session.query(CMSActivity).filter_by(name=name.replace('_','/'), year=year).one()
        return jsonify(activity.to_json())
    except:
        current_app.logger.error( 'no activity found for %s in %s' % (name.replace('_','/'), year) )
        return jsonify( { "error": 'no activity found for %s in %s ' % (name.replace('_','/'), year) } )

@api.route('/activitiesForProjectId/<int:projId>', methods=['GET', 'POST'])
@login_required
def get_activitesForProjectId(projId):

    activities = getTaskActivitiesForProject(projId)
    if not activities:
        current_app.logger.error( 'no activites found for projId %s ' % projId )
        return jsonify( { "error": 'no activities found for projId %s ' % projId } )

    actList = [ ]
    for a in activities :
        actList.append( a.to_json() )

    return jsonify( { 'activities' : actList } )

@api.route('/activityByTaskCode', methods=['POST'])
@login_required
def activityByTaskCode():

    code = request.form.get( 'code', None )

    code = code.replace('_', '/')
    try:
        task = db.session.query(Task).filter_by(code=code).one()
    except Exception as e:
        msg =  'no task found for code %s - got %s ' % (code, str(e))
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    activity = task.activity

    mgrs = set()
    try:
        for m, uName in (db.session.query(Manager, EprUser.name)
                           .filter(Manager.status == 'active')
                           .filter(Manager.itemType == 'activity')
                           .filter(Manager.itemCode == activity.code)
                           .filter(Manager.role==Permission.MANAGEACT)
                           .filter(Manager.userId==EprUser.id)
                           .all()):
           mgrs.add(uName)
    except Exception as e:
        msg = 'ERROR when getting managers for act with task-code %s - got %s ' % (code, str(e))
        current_app.logger.info( msg )
        return jsonify( { "error": msg } )

    return jsonify( {'activity': activity.to_json(),
                     'mgrs' : '<br/>'.join(mgrs),
                     'numMgrs' : len(mgrs),
                     'mgrEmails' : ','.join( getMgrEmails('activity', activity.code) )
                     } )

@api.route('/actTasks/<string:lvl3>', methods=['GET', 'POST'])
@login_required
def actTasks(lvl3):

    selYear = request.args.get('year', getPledgeYear())

    code = request.args.get('aCode', '')
    if code == '':
        code = request.form.get( 'aCode', '' )

    lvl3 = lvl3.replace( '_slash_', '/' )

    current_app.logger.debug( 'actTasks> got %s request for %s/%s in %s' % (request.method, lvl3, code, selYear) )

    if code.strip() == '': return jsonify( { 'data': [] } )

    subQuery = (db.session.query( Task )
                     .join( CMSActivity, CMSActivity.id == Task.activityId )
                     .filter( CMSActivity.code == code )
                     .filter( Task.locked == False )
                     .filter( Task.status == 'ACTIVE' )
                     .filter( Task.year == selYear )
                     .filter( Task.tType != 'InstResp' )
                     .filter( Task.shiftTypeId not in getTaskShiftTypeIds() )
                )
    if lvl3 != 'any-any':
        subQuery = ( subQuery.join(Level3Name, Level3Name.id == Task.level3)
                             .filter( Level3Name.name == lvl3 )
                    )
    try:
        tListDB = subQuery.all()
    except Exception as e:
        return jsonify( { 'data': ['ERROR: could not get task list for lvl3 %s in activity %s -- got %s' % (lvl3, code, str(e)) ] } )

    current_app.logger.info( 'actTasks> task list from DB for code %s raw: %s' % (code, tListDB) )

    tList = []
    for t in tListDB:
        tList.append( { 'name' : t.name, 'code': t.code } )

    current_app.logger.info( 'actTasks> task list for code %s filtered: %s' % (code, tList) )

    return jsonify( {'data' : tList} )


@api.route('/deleteActivity', methods=['POST'])
@login_required
def deleteActivity():

    code    = request.form.get('actCode', None)
    target  = request.form.get('target', None)
    selYear = request.form.get('year', getPledgeYear())

    current_app.logger.info("request to delete activity code %s (year %s)" % (code, selYear) )

    if not (current_user.is_administrator() or \
            current_user.canManage('activity', code, year=selYear) ):
        msg = 'You (%s) are not a manager for this activity, sorry.' % current_user.username
        current_app.logger.error(msg)
        flash(msg, 'error')
        return jsonify( { 'status' : msg } ), 403

    act = db.session.query(CMSActivity).filter_by(code=code, year=selYear).one()
    if act.status == 'DELETED':
        msg = 'Activity %s (%s) already deleted - no action taken.' % (act.name, selYear)
        flash(msg, 'error')
        current_app.logger.warning( msg )
        return jsonify( { 'status' : msg } ), 200

    current_app.logger.info("found activity to delete as code %s : %s " % (act.code, act) )

    if act.tasks:
        activeTasks = db.session.query(Task.id).filter(Task.activityId==act.id).filter(Task.status=='ACTIVE').all()
        current_app.logger.debug( act.tasks, len(activeTasks) )
        if len(activeTasks):
            return jsonify( { 'status' : 'ERROR can not delete activity %s, as there are still tasks associated with it: %s' % (act.name, ','.join( [t.name for t in act.tasks])) } ), 400

    # here we have a valid delete request, update status of task:
    act.status = 'DELETED'
    commitUpdated( act )

    msg = 'Activity %s (%s) successfully deleted' % (act.name, selYear)
    flash( msg )
    current_app.logger.info( msg )

    return jsonify( {'status': msg} ), 200


@api.route('/actLvl3/<string:code>', methods=['GET', 'POST'])
@login_required
def actLvl3(code):

    selYear = request.args.get('year', getPledgeYear())
    current_app.logger.info( 'actLvl3> got request for l3list for code %s in %s' % (code, selYear) )

    if code.strip() == '': return jsonify( { 'data': [] } )

    try:
        l3ListDB = (db.session.query( Level3Name.name.distinct() )
                     .join( Task, Task.level3 == Level3Name.id )
                     .join( CMSActivity, CMSActivity.id == Task.activityId )
                     .filter( CMSActivity.code == code )
                     .filter( Task.locked == False )
                     .filter( Task.status == 'ACTIVE' )
                     .filter( Task.year == selYear )
                     .filter( Task.shiftTypeId not in getTaskShiftTypeIds() )
                     .all()
                    )
    except Exception as e:
        return jsonify( { 'data': ['ERROR: could not get task list for activity %s -- got %s' % (code, str(e)) ] } )

    current_app.logger.info( 'actLvl3> l3list for code %s from DB: %s' % (code, l3ListDB) )

    l3List = []
    l3Seen = set()
    for level3, in sorted(l3ListDB):
        if level3 not in l3Seen and level3 is not None:
            l3Seen.add(level3)
            l3List.append( { 'name' : level3, 'code': level3 } )

    if l3List == []:
        l3List = [ { 'name' : 'Any', 'code': 'any-any' } ]

    current_app.logger.info( 'actLvl3> l3list for code %s filtered: %s' % (code, l3List) )

    return jsonify( {'data' : l3List} )
