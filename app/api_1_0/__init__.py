from flask import Blueprint

api = Blueprint('api', __name__)

from . import authentication, users, errors, tasks, managers, institutes, projects, pledges, activities, countries, shifts, statistics
