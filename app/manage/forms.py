
from datetime import datetime

from flask_wtf import FlaskForm as Form
from wtforms import StringField, SelectField, SelectMultipleField, SubmitField, IntegerField, \
                    TextAreaField, BooleanField, DecimalField, TextField

from wtforms.validators import InputRequired, Optional, Length, Email, Regexp, NumberRange

from .. import db
from ..models import Level3Name

class ProjectManagerForm(Form):

    name       = StringField('Name', validators=[InputRequired()])
    updateName = SubmitField('Update name')

    description = TextAreaField('Description', validators=[InputRequired()])
    update = SubmitField('Update description')

    managers = SelectMultipleField('Managers', coerce=int)
    remove = SubmitField('Remove selected managers')
    addMgrs = SubmitField('Add new managers')
    addMgtEgroup = SubmitField('Add new management eGroup')

class ActivityManagerForm(Form):

    name         = StringField('Name', validators=[InputRequired()])
    updateName = SubmitField('Update name')

    description = TextAreaField('Description', validators=[InputRequired()])
    update = SubmitField('Update description')

    managers = SelectMultipleField('Managers', coerce=int)
    remove = SubmitField('Remove selected managers')
    addMgrs = SubmitField('Add new managers')
    addMgtEgroup = SubmitField('Add new management eGroup')


class TaskManagerForm(Form):

    activities = SelectField( 'Select New Activity/LV2', coerce=int )
    chgAct = SubmitField('Change Activity/LV2')

    level3 = StringField( 'Choose new Level3 Name', validators=[InputRequired()] )
    chgLvl3 = SubmitField('Update LV3 name')

    name         = StringField('Change Name', validators=[InputRequired()])
    updateName = SubmitField('Update name')

    description = TextAreaField('Update Description', validators=[InputRequired()])
    update = SubmitField('Update description')

    pctAtCERN = DecimalField('Required % of presence at CERN', validators=[NumberRange(0., 100., 'between 0 and 100 percent')] )

    # e.g. Perennial, one-off, central shift, expert on call shift
    tType = SelectField('Type')

    locked = BooleanField( 'Locked' )

    comment = TextAreaField('Comment', validators=[Optional(), Length(max=2000)])

    shiftTypeId = SelectField('Shift type')

    # kind = SelectField( 'Kind', choices=[ (v, v) for v in [ 'CORE', 'NON-CORE' ] ] )

    neededWork  = DecimalField('Work needed (person-months)', validators=[InputRequired(),
                                                                          NumberRange(0., 1200.,'at least one month, max. 1200 months')])
    submit = SubmitField('Submit')

    managers = SelectMultipleField('Managers', coerce=int)
    remove = SubmitField('Remove selected managers')
    addMgrs = SubmitField('Add new managers')
    addMgtEgroup = SubmitField('Add new management eGroup')

    observers = SelectMultipleField('Observers', coerce=int)
    removeObs = SubmitField('Remove selected observers')
    addObs = SubmitField('Add new observers')

class AddUserForm(Form):

    username = StringField('CERN username')
    name  = StringField('Name, Firstname', validators=[InputRequired()])
    hrId  = IntegerField('hrId', validators=[InputRequired()])
    cmsId = IntegerField('cmsId', validators=[InputRequired()])
    instCode = StringField('CMS Institute Code', validators=[InputRequired()])
    status = SelectField('Status', choices=[("CMS","CMS"),("Other","Other")], default=("CMS","CMS"))
    category = SelectField('Status',
                           choices=[("Physicist","Physicist"),
                                    ("Doctoral Student", "Doctoral Student"),
                                    ("Theoretical Physicist","Theoretical Physicist")],
                           default=("Physicist","Physicist"))
    authorReq = BooleanField('Request for Authorship', default=False)
    isSusp = BooleanField('Is Suspended', default=False)
    comment = StringField('comment', validators=[Optional()])

    addUser = SubmitField('Add User')

class AddInstituteForm(Form):

    name    = StringField('Name', validators=[InputRequired()])
    country = StringField('Country', validators=[InputRequired()])
    code    = StringField('CMS Institute Code', validators=[InputRequired()])
    cernId  = IntegerField('CERN institute id', validators=[InputRequired()])
    status  = StringField('Status', validators=[InputRequired()])
    comment = StringField('Comment', validators=[Optional()])

    addInst = SubmitField('Add Institute')

class EditUserForm(Form):

    username = StringField('CERN username')
    name  = StringField('Name, Firstname', validators=[InputRequired()])
    instCode = StringField('CMS Institute Code', validators=[InputRequired()])
    status = SelectField('Status', choices=[("CMS","CMS"),("Other","Other")], default=("CMS","CMS"))
    category = SelectField('Category',
                           choices=[("Physicist","Physicist"),
                                    ("Doctoral Student", "Doctoral Student"),
                                    ("Theoretical Physicist","Theoretical Physicist")],
                           default=("Physicist","Physicist"))
    comment = StringField('comment', validators=[Optional()])

    addUser = SubmitField('Submit changes')

