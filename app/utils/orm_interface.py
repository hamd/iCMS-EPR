from flask_sqlalchemy import SQLAlchemy, SignallingSession, get_state
from icms_orm.orm_interface import make_declarative_base, QueryModel, ManagerMixin
from icms_orm.orm_interface import BindKeyExtractor
from sqlalchemy import orm
from icms_orm import IcmsDeclarativeBasesFactory
from sqlalchemy import MetaData
import flask
from alchy.model import extend_declarative_base
import logging


class IcmsSignallingSession(SignallingSession):
    def get_bind(self, mapper=None, clause=None):
        bind_key = BindKeyExtractor.safely_extract_from_any(mapper, clause)
        if bind_key is not None:
            state = get_state(self.app)
            return state.db.get_engine(self.app, bind=bind_key)
        return SignallingSession.get_bind(self, mapper=mapper, clause=clause)


class WebappOrmManager(SQLAlchemy, ManagerMixin):
    """Flask extension that integrates alchy with Flask-SQLAlchemy."""

    def __init__(self, app=None, use_native_unicode=True, session_options=None, Model=None, metadata=None):
        if session_options is None:
            session_options = {}

        session_options.setdefault('query_cls', QueryModel)
        metadata = metadata or MetaData()
        super(WebappOrmManager, self).__init__(app, use_native_unicode, session_options, metadata=metadata)

        self.Query = session_options['query_cls']

        for cls in IcmsDeclarativeBasesFactory.base_classes():
            logging.debug('Extending declarative base: %s', cls.__name__)
            extend_declarative_base(cls, self.session)
    
    def create_session(self, options):
        return orm.sessionmaker(class_=IcmsSignallingSession, db=self, **options)

    def init_app(self, app):
        bind_keys = app.config['SQLALCHEMY_BINDS'].keys()
        for bind_key in bind_keys:
            _base = IcmsDeclarativeBasesFactory.get_for_bind_key(bind_key)
            extend_declarative_base(_base, self.session)
        super().init_app(app)

    def make_declarative_base(self, *args, **kwargs):
        """
        There have been certain interfacing issues hence we'll just look for a MetaData instance in all (kw)args
        """
        metadata = None
        for _container in (args, kwargs.values()):
            for _arg in _container:
                if  isinstance(_arg, MetaData):
                    metadata = _arg

        return make_declarative_base(self.session, metadata=metadata or self.metadata)

    def __getattr__(self, attr):
        """Delegate all other attributes to self.session"""
        return getattr(self.session, attr)
