
<!-- see also: https://docs.gitlab.com/ce/user/project/pipelines/settings.html#badges -->

**Pipeline status:** [![pipeline status](https://gitlab.cern.ch/pfeiffer/iCMS-EPR/badges/master/pipeline.svg)](https://gitlab.cern.ch/pfeiffer/iCMS-EPR/commits/master)
**Coverage report:** [![Coverage report](https://gitlab.cern.ch/pfeiffer/iCMS-EPR/badges/master/coverage.svg)](https://gitlab.cern.ch/pfeiffer/iCMS-EPR/coverage/master) 
-- [details](https://cern.ch/icmsweb-pages/epr/coverage_html_report/index.html)

iCMS - EPR
==========

The new EPR tool for CMS.


Documentation
=============

An updated version (Oct 4, 2016) of the Users Guide is available at: 

    https://cms-docdb.cern.ch/cgi-bin/DocDB/ShowDocument?docid=12808


Issue Reporting
===============

Please open a [JIRA ticket](https://its.cern.ch/jira/browse/ICMSEPR/?selectedTab=com.atlassian.jira.jira-projects-plugin:summary-panel) or simply [send an email](mailto:icms-support@cern.ch)

Testing
=======

Run the `combinedCoverage.sh` script to run both the unit tests and 
the Selenium based testing and combine the results of the coverage.


