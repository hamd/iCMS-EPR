#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import datetime

from app.models import TimeLineUser, TimeLineInst
from sqlalchemy.sql import expression, func
from sqlalchemy.ext import compiler
from sqlalchemy import and_, or_, orm, case

class group_concat(expression.FunctionElement):
    name = "group_concat"


@compiler.compiles(group_concat, 'mysql')
def _group_concat_mysql(element, compiler, **kw):
    if len(element.clauses) == 2:
        separator = compiler.process(element.clauses.clauses[1])
    else:
        separator = ','

    return 'GROUP_CONCAT(%s SEPARATOR %s)'.format(
        compiler.process(element.clauses.clauses[0]),
        separator,
    )


@compiler.compiles(group_concat, 'oracle')
def _group_concat_oracle(element, compiler, **kw):

    return "LISTAGG(%s, ', ') WITHIN GROUP (ORDER BY %s)" % (
        compiler.process(element.clauses.clauses[0]), compiler.process(element.clauses.clauses[0]))


@compiler.compiles(group_concat, 'postgresql')
def _group_concat_postgresql(element, compiler, **kw):

    return "STRING_AGG(%s, ', ' ORDER BY %s)" % ( compiler.process(element.clauses.clauses[0]), compiler.process(element.clauses.clauses[0]) )


class QueryFactory(object):

    session = None

    @staticmethod
    def init_with_session(session):
        QueryFactory.session = session

    @staticmethod
    def q_time_line_user(year=None, inst_code=None, status=None):
        q = QueryFactory.session.query(
            TimeLineUser.cmsId.label('cmsId'),
            func.max(TimeLineUser.timestamp).label('laststamp'),
            func.min(TimeLineUser.timestamp).label('firststamp'),
            func.sum(TimeLineUser.dueApplicant).label('dueApplicant'),
            func.sum(TimeLineUser.dueAuthor).label('dueAuthor'),
            func.sum( TimeLineUser.yearFraction ).label( 'yearFraction' ),
            (func.min( TimeLineUser.timestamp ) + datetime.timedelta(days=365) * func.sum( TimeLineUser.yearFraction )).label('endDate' )
        )
        if year is not None:
            q = q.filter(TimeLineUser.year == year)
        if inst_code is not None:
            q = q.filter(TimeLineUser.instCode == inst_code)
        if status is not None:
            if '%' in status:
                q = q.filter(TimeLineUser.status.like(status))
            else:
                q = q.filter(TimeLineUser == status)
        q = q.group_by(TimeLineUser.cmsId)
        return q

    @staticmethod
    def sq_time_line_user(year, inst_code=None, status=None):
        return QueryFactory.q_time_line_user(year=year, inst_code=inst_code, status=status).subquery()

    @staticmethod
    def q_time_line_inst(year=None, inst_code=None):
        sq = QueryFactory.session.query(
            TimeLineInst.code.label('code'),
            func.max(TimeLineInst.timestamp).label('laststamp'),
            func.min(TimeLineInst.timestamp).label('firststamp')
        )
        if year is not None:
            sq = sq.filter(TimeLineInst.year == year)
        if inst_code is not None:
            sq = sq.filter(TimeLineInst.code == inst_code)
        sq = sq.group_by(TimeLineInst.code)
        return sq

    @staticmethod
    def sq_time_line_inst(year, inst_code=None):
        return QueryFactory.q_time_line_inst(year=year, inst_code=inst_code).subquery()

    @staticmethod
    def q_total_dues_by_inst(year, inst_code=None):
        sq_user_last_line = QueryFactory.sq_get_user_last_line(year=year, inst_code=inst_code)
        tl_alias = orm.aliased(TimeLineUser, sq_user_last_line)

        sq = QueryFactory.session.query(
                TimeLineUser.instCode.label('instCode'),
                func.sum(TimeLineUser.dueAuthor).label('duesAuthor'),
                func.sum(TimeLineUser.dueApplicant).label('duesApplicant'),
                func.sum(TimeLineUser.yearFraction * case( { True : 1.0 }, value=TimeLineUser.isAuthor, else_ = 0. ) ).label( 'nAuthors' )\
            )\
            .join(tl_alias, TimeLineUser.cmsId == tl_alias.cmsId) \
            .filter(or_(TimeLineUser.dueAuthor > 0, tl_alias.status != 'EXMEMBER')) \
            .filter(TimeLineUser.year == year)\
            .filter(TimeLineUser.status == 'CMS')
        if inst_code is not None:
            sq = sq.filter(TimeLineUser.instCode == inst_code)
        sq = sq.group_by(TimeLineUser.instCode)
        return sq

    @staticmethod
    def sq_total_dues_by_inst(year, inst_code=None):
        return QueryFactory.q_total_dues_by_inst(year=year, inst_code=inst_code).subquery()

    @staticmethod
    def q_get_user_last_line(year = None, cms_id = None, inst_code = None):
        """
        Picks the last timeline for a user within a given year
        :param year:
        :param cms_id:
        :param inst_code:
        :return:
        """
        sq = QueryFactory.session.query(
            TimeLineUser.cmsId.label('cmsId'),
            func.max(TimeLineUser.timestamp).label('laststamp'),
        )
        if year is not None:
            sq = sq.filter(TimeLineUser.year == year)
        if inst_code is not None:
            sq = sq.filter(TimeLineUser.instCode == inst_code)
        if cms_id is not None:
            sq = sq.filter(TimeLineUser.cmsId == cms_id)
        sq = sq.group_by(TimeLineUser.cmsId).subquery()

        q = QueryFactory.session.query(TimeLineUser).join(sq, and_(TimeLineUser.cmsId == sq.c.cmsId, TimeLineUser.timestamp == sq.c.laststamp))
        if year is not None:
            q = q.filter(TimeLineUser.year == year)
        if cms_id is not None:
            q = q.filter(TimeLineUser.cmsId == cms_id)
        if inst_code is not None:
            q = q.filter(TimeLineUser.instCode == inst_code)
        return q

    @staticmethod
    def sq_get_user_last_line(year = None, cms_id = None, inst_code = None):
        return QueryFactory.q_get_user_last_line(year=year, cms_id=cms_id, inst_code=inst_code).subquery()

    @staticmethod
    def q_get_latest_inst(year=None, inst_code=None):
        sq = QueryFactory.sq_time_line_inst(year=year, inst_code=inst_code)
        q = QueryFactory.session.query(TimeLineInst).join(sq, and_(TimeLineInst.code == sq.c.code, TimeLineInst.timestamp == sq.c.laststamp))
        if inst_code is not None:
            q = q.filter(TimeLineInst.code == inst_code)
        return q
